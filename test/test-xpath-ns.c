/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*- */
/* test-xpath.c
 *
 * Copyright (C) 2006 G.S.Barbieri <Gustavo.Barbieri@indt.org.br>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <gdome.h>
#include <gdome-xpath.h>
#include <stdio.h>
#include <string.h>

static char uri[] = "http://www.indt.org.br";
static char xml[] = \
"<doc xmlns=\"http://www.indt.org.br\">" \
"<a><b><p attr=\"name1\">value1.1</p><p attr=\"name2\">value1.2</p></b></a>" \
"<d><p attr=\"name1\">value2</p></d>" \
"</doc>";

GdomeXPathResult *
xpath_node( const char *expr, const char *prefix, const char *uri,
	    GdomeNode *context, GdomeException *exc )
{
  GdomeException clean_exc = GDOME_NOEXCEPTION_ERR;
  GdomeXPathResult *res = NULL;
  GdomeXPathEvaluator *eval = NULL;
  GdomeDOMString *s = NULL, *s_prefix = NULL, *s_uri = NULL;
  GdomeDocument *doc = NULL;
  GdomeElement *root = NULL;
  GdomeXPathNSResolver *nsresolv = NULL;

  if ( gdome_n_nodeType( context, exc ) == GDOME_DOCUMENT_NODE )
    {
      doc = (GdomeDocument*)context;
      gdome_doc_ref( doc, exc );
    }
  else
    doc = gdome_n_ownerDocument( context, exc );

  if ( *exc ) goto clean_up;

  root = gdome_doc_documentElement( doc, exc );
  if ( *exc ) goto clean_up;

  eval = gdome_xpeval_mkref();

  g_assert( eval );

  nsresolv = gdome_xpeval_createNSResolver( eval, (GdomeNode*)root, exc );
  if ( *exc ) goto clean_up;

  s_prefix = gdome_str_mkref( prefix );
  s_uri = gdome_str_mkref( uri );

  gdome_xpnsresolv_registerNs( nsresolv, s_prefix, s_uri, exc );
  if ( *exc ) goto clean_up;

  s = gdome_str_mkref( expr );

  g_assert( s );

  res = gdome_xpeval_evaluate( eval, s, context, nsresolv, 0, NULL, exc );
  if ( *exc ) goto clean_up;

 clean_up:
  if ( s )
    gdome_str_unref( s );

  if ( s_prefix )
    gdome_str_unref( s_prefix );

  if ( s_uri )
    gdome_str_unref( s_uri );

  if ( eval )
    gdome_xpeval_unref( eval, &clean_exc );

  if ( nsresolv )
    gdome_xpnsresolv_unref( nsresolv, &clean_exc );

  if ( root )
    gdome_el_unref( root, &clean_exc );

  if ( doc )
    gdome_doc_unref( doc, &clean_exc );

  return res;
}


GdomeXPathResult *
xpath_xml( const char *expr, const char *prefix, const char *uri,
	   char *xml, GdomeDocument *doc, GdomeException *exc )
{
  GdomeException clean_exc = GDOME_NOEXCEPTION_ERR;
  GdomeDOMImplementation *domimpl = NULL;
  GdomeXPathResult *res = NULL;

  domimpl = gdome_di_mkref();
  doc = gdome_di_createDocFromMemory( domimpl, xml, GDOME_LOAD_PARSING, exc );
  if ( *exc ) goto clean_up;

  res = xpath_node( expr, prefix, uri, (GdomeNode*)doc, exc );
  if ( *exc ) goto clean_up;

 clean_up:
  if ( domimpl )
    gdome_di_unref( domimpl, &clean_exc );

  return res;
}

int
test_root( void )
{
  GdomeException exc = GDOME_NOEXCEPTION_ERR;
  GdomeDocument *doc = NULL;
  GdomeXPathResult *res = xpath_xml( "/", "ns", uri, xml, doc,  &exc );
  GdomeNode *n = NULL;
  GdomeDOMString *s = NULL;
  unsigned i = 0;

  g_assert( res );

  while ( ( n = gdome_xpresult_iterateNext( res, &exc ) ) != NULL )
    {
      s = gdome_n_nodeName( n, &exc );
      if ( exc ) goto clean_up;

      g_assert( strcmp( "#document", s->str ) == 0 );

      gdome_str_unref( s );
      s = NULL;

      gdome_n_unref( n, &exc );
      if ( exc ) goto clean_up;
      n = NULL;

      i++;
    }

  g_assert( i == 1 );


 clean_up:
  if ( exc )
    fprintf( stderr, "Exception: %d\n", exc );

  if ( s )
    gdome_str_unref( s );

  if ( n )
    gdome_n_unref( n, &exc );

  if ( res )
    gdome_xpresult_unref( res, &exc );

  if ( doc )
    gdome_doc_unref( doc, &exc );

  return exc;
}


int
test_complete( void )
{
  GdomeException exc = GDOME_NOEXCEPTION_ERR;
  GdomeDocument *doc = NULL;
  GdomeXPathResult *res = xpath_xml( "//ns:a/ns:b/ns:p[@attr='name1']/text()",
				     "ns", uri, xml, doc,  &exc );
  GdomeNode *n = NULL;
  GdomeDOMString *s = NULL;
  unsigned i = 0;

  g_assert( res );

  while ( ( n = gdome_xpresult_iterateNext( res, &exc ) ) != NULL )
    {
      s = gdome_t_data( (GdomeText*)n, &exc );
      if ( exc ) goto clean_up;

      g_assert( strcmp( s->str, "value1.1" ) == 0 );

      gdome_str_unref( s );
      s = NULL;

      gdome_n_unref( n, &exc );
      if ( exc ) goto clean_up;
      n = NULL;

      i++;
    }

  g_assert( i == 1 );


 clean_up:
  if ( exc )
    fprintf( stderr, "Exception: %d\n", exc );

  if ( s )
    gdome_str_unref( s );

  if ( n )
    gdome_n_unref( n, &exc );

  if ( res )
    gdome_xpresult_unref( res, &exc );

  if ( doc )
    gdome_doc_unref( doc, &exc );

  return exc;
}


int
test_relative( void )
{
  GdomeException exc = GDOME_NOEXCEPTION_ERR;
  GdomeDocument *doc = NULL;
  GdomeXPathResult *res = NULL, *res2 = NULL;
  GdomeNode *n = NULL, *n2 = NULL;
  GdomeDOMString *s=NULL, *s2 = NULL;
  unsigned i = 0, j = 0;

  res = xpath_xml( "//ns:a/ns:b", "ns", uri, xml, doc,  &exc );
  if ( exc ) goto clean_up;

  g_assert( res );

  while ( ( n = gdome_xpresult_iterateNext( res, &exc ) ) != NULL )
    {
      s = gdome_n_nodeName( n, &exc );
      if ( exc ) goto clean_up;

      g_assert( strcmp( s->str, "b" ) == 0 );

      gdome_str_unref( s );
      s = NULL;

      res2 = xpath_node( "ns:p[@attr=\"name2\"]/text()", "ns", uri, n, &exc );
      if ( exc ) goto clean_up;

      g_assert( res2 );

      while ( ( n2 = gdome_xpresult_iterateNext( res2, &exc ) ) != NULL )
	{
	  s2 = gdome_t_data( (GdomeText*)n2, &exc );
	  if ( exc ) goto clean_up;

	  g_assert( strcmp( s2->str, "value1.2" ) == 0 );

	  gdome_str_unref( s2 );
	  s2 = NULL;

	  gdome_n_unref( n2, &exc );
	  if ( exc ) goto clean_up;
	  n2 = NULL;

	  j ++;
	}


      gdome_n_unref( n, &exc );
      if ( exc ) goto clean_up;
      n = NULL;

      i++;
    }

  g_assert( i == 1 );
  g_assert( j == 1 );


 clean_up:
  if ( exc )
    fprintf( stderr, "Exception: %d\n", exc );

  if ( s )
    gdome_str_unref( s );

  if ( s2 )
    gdome_str_unref( s2 );

  if ( n )
    gdome_n_unref( n, &exc );

  if ( n2 )
    gdome_n_unref( n2, &exc );

  if ( res )
    gdome_xpresult_unref( res, &exc );

  if ( res2 )
    gdome_xpresult_unref( res2, &exc );

  if ( doc )
    gdome_doc_unref( doc, &exc );

  return exc;
}


int
test_multiple( void )
{
  GdomeException exc = GDOME_NOEXCEPTION_ERR;
  GdomeDocument *doc = NULL;
  GdomeXPathResult *res = xpath_xml( "//ns:p[@attr=\"name1\"]/text()",
				     "ns", uri, xml, doc,  &exc );
  GdomeNode *n = NULL;
  GdomeDOMString *s = NULL;
  static const char *values[] = { "value1.1", "value2" };
  unsigned i = 0, max = 2;

  g_assert( res );

  while ( ( n = gdome_xpresult_iterateNext( res, &exc ) ) != NULL )
    {
      s = gdome_t_data( (GdomeText*)n, &exc );
      if ( exc ) goto clean_up;

      g_assert( i < max );

      g_assert( strcmp( s->str, values[ i ] ) == 0 );

      gdome_str_unref( s );
      s = NULL;

      gdome_n_unref( n, &exc );
      if ( exc ) goto clean_up;
      n = NULL;

      i++;
    }

  g_assert( i == 2 );


 clean_up:
  if ( exc )
    fprintf( stderr, "Exception: %d\n", exc );

  if ( s )
    gdome_str_unref( s );

  if ( n )
    gdome_n_unref( n, &exc );

  if ( res )
    gdome_xpresult_unref( res, &exc );

  if ( doc )
    gdome_doc_unref( doc, &exc );

  return exc;
}




int
main( int argc, char *argv[] )
{
  test_root();
  test_complete();
  test_relative();
  test_multiple();
  return 0;
}
