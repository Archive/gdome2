#include <stdio.h>
#include <gtk/gtk.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <gdome.h>
#include <gdome-xml-document.h>


void quit_cb (void)
{
  gtk_main_quit ();
}


void delete_event_cb (void)
{
  /* delete something from the DOM tree
     to trigger an event */

}

void insert_event_cb (void)
{
  /* delete something from the DOM tree
     to trigger an event */

}


 /* create the sample simple 
     DOM tree here... */
void create_dom_tree (char *file)
{
  xmlDocPtr doc;
  GdomeDocument *gdoc;

  doc = xmlParseFile (file);
  gdoc = gdome_xml_from_document (doc);


  /* connect a listener somewhere on the tree */

}





int main (int argc, char **argv)
{
  GtkWidget *window;
  GtkWidget *vbox;
  GtkWidget *button;
  
  gtk_init (&argc, &argv);

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		      GTK_SIGNAL_FUNC (quit_cb), NULL);

  vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (window), vbox);

  button = gtk_button_new_with_label ("Quit");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (quit_cb), NULL);
  gtk_box_pack_start (GTK_BOX (vbox), button, 0, 0, 0);

  button = gtk_button_new_with_label ("DeleteEvent");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (delete_event_cb), NULL);
  gtk_box_pack_start (GTK_BOX (vbox), button, 0, 0, 0);

  button = gtk_button_new_with_label ("InsertEvent");
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
		      GTK_SIGNAL_FUNC (insert_event_cb), NULL);
  gtk_box_pack_start (GTK_BOX (vbox), button, 0, 0, 0);

  gtk_widget_show_all (GTK_WIDGET (window));


  create_dom_tree ("test.xml");

  gtk_main ();
  return 0;

}
