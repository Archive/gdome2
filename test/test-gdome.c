/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*- */
/* test-gdome.c
 *
 * Copyright (C) 1999 Raph Levien <raph@acm.org>
 * Copyright (C) 2000 Mathieu Lacage <mathieu@gnu.org>
 * Copyright (C) 2000 Anders Carlsson <andersca@gnu.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdio.h>
#include <gtk/gtk.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <gdome.h>
#include <gdome-util.h>
#include <gdome-xml-element.h>
#include <gdome-xml-str.h>
#include <gdome-xml-document.h>
#include <gdome-xml-domimpl.h>

/* Basically, this test will read the libxml tree of Elements
   then, it will check the gdome_el_*_methods functions return
   the right answer.
   Then, it tries to modify the Element Nodes by:
   - adding a property -> checks it is done in libxml.
   - adding a child -> checks it is done in libxml.
   This function is recursive.
*/


void
test_DOMImplementation (void)
{
	GdomeDOMImplementation *impl;
	GdomeDOMImplementation *impl2;

	/* There should never be more than one DOMImplementation */
	
	impl = gdome_xml_DOMImplementation_mkref ();
	
	impl2 = gdome_xml_DOMImplementation_mkref ();

	if (impl != impl2)
		g_warning ("DOMImplementation error");
}

void
test_doc_structure (GdomeNode *n)
{
	GdomeException exc = 0;
	xmlNode *node = (xmlNode *)((Gdome_xml_Element *)n)->n;
	
	gdome_xml_doc_getElementsByTagName ((GdomeDocument *)n, gdome_xml_str_mkref_own ("p"), &exc);
}

void
test_el_structure (GdomeNode *n)
{
    xmlNode *node = (xmlNode *)((Gdome_xml_Element *)n)->n;
    GdomeException exc = 0;
    GdomeDOMString *attr_name;
    GdomeDOMString *attr_value;

    /* check consistency in gdome. */
    if (gdome_n_nodeType (GDOME_N(n), &exc) !=
				gdome_el_nodeType (GDOME_EL(n), &exc))
			g_warning ("Node::nodeType, error.");
    if (gdome_n_hasChildNodes (GDOME_N(n), &exc) != 
				gdome_el_hasChildNodes (GDOME_EL(n), &exc))
			g_warning ("Node::hasChildNodes, error.");
    if (gdome_n_parentNode (GDOME_N(n), &exc) != 
				gdome_el_parentNode (GDOME_EL(n), &exc))
			g_warning ("Node::parentNode, error.");
    /* note: the following check cannot succeed because 
       of the implementation */
    if (gdome_n_childNodes (GDOME_N(n), &exc) != 
				gdome_el_childNodes (GDOME_EL(n), &exc))
			g_warning ("Node::childNodes, error.");
    if (gdome_n_firstChild (GDOME_N(n), &exc) != 
				gdome_el_firstChild (GDOME_EL(n), &exc))
			g_warning ("Node::firstChild, error.");
    if (gdome_n_lastChild (GDOME_N(n), &exc) != 
				gdome_el_lastChild (GDOME_EL(n), &exc))
			g_warning ("Node::lastChild, error.");
    if (gdome_n_previousSibling (GDOME_N(n), &exc) !=
				gdome_el_previousSibling (GDOME_EL(n), &exc))
			g_warning ("Node::previousSibling, error.");
    if (gdome_n_nextSibling (GDOME_N(n), &exc) !=
				gdome_el_nextSibling (GDOME_EL(n), &exc))
			g_warning ("Node::nextSibling, error.");
    /* note: the following check cannot succeed because 
       of the implementation */
    if (gdome_n_attributes (GDOME_N(n), &exc) !=
				gdome_el_attributes (GDOME_EL(n), &exc))
			g_warning ("Node::attributes, error.");
    if (gdome_n_ownerDocument (GDOME_N(n), &exc) !=
				gdome_el_ownerDocument (GDOME_EL(n), &exc))
			g_warning ("Node::ownerDocument, error.");
    if (strcmp (gdome_n_nodeName (GDOME_N(n), &exc)->str,
								gdome_el_nodeName (GDOME_EL(n), &exc)->str) != 0)
			g_warning ("Node::nodeName, error.");    
    if (gdome_n_nodeValue (GDOME_N(n), &exc) != NULL ||
				gdome_el_nodeValue (GDOME_EL(n), &exc) != NULL)
			g_warning ("Node::nodeValue, error.");


    /* check consistency between libxml and gdome */
    if (strcmp (gdome_n_nodeName (GDOME_N(n), &exc)->str, node->name) != 0)
			g_warning ("Node::nodeName, error.");
    if ((gdome_n_nodeType (GDOME_N(n), &exc) == GDOME_ELEMENT_NODE &&
				 node->type != XML_ELEMENT_NODE) ||
				(gdome_n_nodeType (GDOME_N(n), &exc) != GDOME_ELEMENT_NODE &&
				 node->type == XML_ELEMENT_NODE)) 
			g_warning ("Node::nodeType, error.");
    if (gdome_n_parentNode (GDOME_N(n), &exc) != gdome_xml_n_mkref (node->parent))
			g_warning ("Node::parentNode, error.");
    if (gdome_n_firstChild (GDOME_N(n), &exc) != gdome_xml_n_mkref (node->children))
			g_warning ("Node::firstChild, error.");
    if (gdome_n_lastChild (GDOME_N(n), &exc) != gdome_xml_n_mkref (node->last))
			g_warning ("Node::lastChild, error.");
    if (gdome_n_previousSibling (GDOME_N(n), &exc) != gdome_xml_n_mkref (node->prev))
			g_warning ("Node::previousSibling, error.");
    if (gdome_n_nextSibling (GDOME_N(n), &exc) != gdome_xml_n_mkref (node->next))
			g_warning ("Node::nextSibling, error.");
    if (gdome_n_ownerDocument (GDOME_N(n), &exc) != gdome_xml_n_mkref (node->doc))
			g_warning ("Node::ownerDocument, error.");
    if ((gdome_n_hasChildNodes(GDOME_N(n), &exc) && node->children == NULL) ||
				(!gdome_n_hasChildNodes(GDOME_N(n), &exc) && node->children != NULL))
			g_warning ("Node::hasChildNodes, error.");


    /* test specific Element properties */
    if (strcmp (gdome_n_nodeName (GDOME_N(n), &exc)->str, 
								gdome_el_tagName (GDOME_EL(n), &exc)->str) != 0)
			g_warning ("Element::tagName, error.");
    
    /* test the get/set/removeAttribute methods */
    attr_name = gdome_xml_str_mkref_nop ("Mathieu");
    attr_value = gdome_xml_str_mkref_nop ("Lacage");
    gdome_el_setAttribute (GDOME_EL(n), attr_name, attr_value, &exc);
    if (strcmp (gdome_el_getAttribute (GDOME_EL(n), attr_name, &exc)->str,
								attr_value->str) != 0)
			g_warning ("Element::get/setAttribute, error.");
    gdome_el_removeAttribute (GDOME_EL(n), attr_name, &exc);
    if (gdome_el_getAttribute (GDOME_EL(n), attr_name, &exc) != NULL)
			g_warning ("Element::removeAttribute, error.");
		
    

}



int
main (int argc, char **argv)
{
  xmlDocPtr doc = NULL;
  GdomeDocument *gdoc = NULL;
  GdomeElement *el = NULL;
  GdomeException exc = 0;
  xmlNode *n = NULL;

  if (argc < 2)
    {
      fprintf (stderr, "usage: test-gdome test.xml\n");
      exit (1);
    }

  test_DOMImplementation ();
  
  doc = xmlParseFile (argv[1]);
  gdoc = gdome_xml_from_document (doc);
  test_doc_structure (gdoc);
  el = gdome_doc_documentElement (gdoc, &exc);
  gdome_doc_unref (gdoc, &exc);
  test_el_structure ((GdomeNode *)el);
  gdome_el_unref (el, &exc);

  xmlFreeDoc (doc);
  return 0;
}

