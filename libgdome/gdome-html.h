/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* gdome-html.h
 *
 * Copyright (C) 1999 Raph Levien <raph@acm.org>
 * Copyright (C) 2000 Mathieu Lacage <mathieu@gnu.org>
 * Copyright (C) 2000 Anders Carlsson <andersca@gnu.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* ----- gdome-html.h ----- */
#ifndef GDOME_HTML_H
#define GDOME_HTML_H

#include "gdome.h"

typedef struct _GdomeHTMLCollection GdomeHTMLCollection;
typedef struct _GdomeHTMLDOMImplementation GdomeHTMLDOMImplementation;
typedef struct _GdomeHTMLDocument GdomeHTMLDocument;
typedef struct _GdomeHTMLElement GdomeHTMLElement;

typedef struct _GdomeHTMLDOMImplementationVtab GdomeHTMLDOMImplementationVtab;
typedef struct _GdomeHTMLCollectionVtab GdomeHTMLCollectionVtab;
typedef struct _GdomeHTMLDocumentVtab GdomeHTMLDocumentVtab;
typedef struct _GdomeHTMLElementVtab GdomeHTMLElementVtab;

struct _GdomeHTMLDOMImplementation {
	const GdomeHTMLDOMImplementationVtab *vtab;
};

struct _GdomeHTMLCollection {
  const GdomeHTMLCollectionVtab *vtab;
};

struct _GdomeHTMLDocument {
  const GdomeHTMLDocumentVtab *vtab;
};

struct _GdomeHTMLElement {
  const GdomeHTMLElementVtab *vtab;
};

/* virtual tables */

struct _GdomeHTMLCollectionVtab {
  void (*ref) (GdomeHTMLCollection *self, GdomeException *exc);
  void (*unref) (GdomeHTMLCollection *self, GdomeException *exc);
  void * (*query_interface) (GdomeHTMLCollection *self, const char *interface, GdomeException *exc);
  unsigned long (*length) (GdomeHTMLCollection *self, GdomeException *exc);
  GdomeNode *(*item) (GdomeHTMLCollection *self, unsigned long index, GdomeException *exc);
  GdomeNode *(*namedItem) (GdomeHTMLCollection *self, GdomeDOMString *name, GdomeException *exc);
};

struct _GdomeHTMLElementVtab {
  GdomeElementVtab super;
  GdomeDOMString *(*id) (GdomeHTMLElement *self, GdomeException *exc);
  void (*set_id) (GdomeHTMLElement *self, GdomeDOMString *id, GdomeException *exc);
  GdomeDOMString *(*title) (GdomeHTMLElement *self, GdomeException *exc);
  void (*set_title) (GdomeHTMLElement *self, GdomeDOMString *title, GdomeException *exc);
  GdomeDOMString *(*lang) (GdomeHTMLElement *self, GdomeException *exc);
  void (*set_lang) (GdomeHTMLElement *self, GdomeDOMString *lang, GdomeException *exc);
  GdomeDOMString *(*dir) (GdomeHTMLElement *self, GdomeException *exc);
  void (*set_dir) (GdomeHTMLElement *self, GdomeDOMString *dir, GdomeException *exc);
  GdomeDOMString *(*className) (GdomeHTMLElement *self, GdomeException *exc);
  void (*set_className) (GdomeHTMLElement *self, GdomeDOMString *className, GdomeException *exc);
};

struct _GdomeHTMLDOMImplementationVtab {
	GdomeDOMImplementationVtab super;
	GdomeHTMLDocument *(*createHTMLDocument) (GdomeHTMLDOMImplementation *self, GdomeDOMString *title, GdomeException *exc);
};

struct _GdomeHTMLDocumentVtab {
  GdomeDocumentVtab super;
  GdomeDOMString *(*title) (GdomeHTMLDocument *self, GdomeException *exc);
  void (*set_title) (GdomeHTMLDocument *self, GdomeDOMString *title, GdomeException *exc);
  GdomeDOMString *(*referrer) (GdomeHTMLDocument *self, GdomeException *exc);
  GdomeDOMString *(*domain) (GdomeHTMLDocument *self, GdomeException *exc);
  GdomeDOMString *(*URL) (GdomeHTMLDocument *self, GdomeException *exc);
  GdomeHTMLElement *(*body) (GdomeHTMLDocument *self, GdomeException *exc);
  void (*set_body) (GdomeHTMLDocument *self, GdomeHTMLElement *body, GdomeException *exc);
  GdomeHTMLCollection *(*images) (GdomeHTMLDocument *self, GdomeException *exc);
  GdomeHTMLCollection *(*applets) (GdomeHTMLDocument *self, GdomeException *exc);
  GdomeHTMLCollection *(*links) (GdomeHTMLDocument *self, GdomeException *exc);
  GdomeHTMLCollection *(*forms) (GdomeHTMLDocument *self, GdomeException *exc);
  GdomeHTMLCollection *(*anchors) (GdomeHTMLDocument *self, GdomeException *exc);
  GdomeDOMString *(*cookie) (GdomeHTMLDocument *self, GdomeException *exc);
  void (*set_cookie) (GdomeHTMLDocument *self, GdomeDOMString *cookie, GdomeException *exc);
  void (*open) (GdomeHTMLDocument *self, GdomeException *exc);
  void (*close) (GdomeHTMLDocument *self, GdomeException *exc);
  void (*write) (GdomeHTMLDocument *self, GdomeDOMString *text, GdomeException *exc);
  void (*writeln) (GdomeHTMLDocument *self, GdomeDOMString *text, GdomeException *exc);
  GdomeNodeList *(*getElementsByName) (GdomeHTMLDocument *self, GdomeDOMString *elementName, GdomeException *exc);
};

void gdome_HTMLCollection_ref (GdomeHTMLCollection *self, GdomeException *exc);
void gdome_HTMLCollection_unref (GdomeHTMLCollection *self, GdomeException *exc);
void * gdome_HTMLCollection_query_interface (GdomeHTMLCollection *self, const char *interface, GdomeException *exc);
unsigned long gdome_HTMLCollection_length (GdomeHTMLCollection *self, GdomeException *exc);
GdomeNode *gdome_HTMLCollection_item (GdomeHTMLCollection *self, unsigned long index, GdomeException *exc);
GdomeNode *gdome_HTMLCollection_namedItem (GdomeHTMLCollection *self, GdomeDOMString *name, GdomeException *exc);

GdomeDOMString *gdome_HTMLElement_id (GdomeHTMLElement *self, GdomeException *exc);
void gdome_HTMLElement_set_id (GdomeHTMLElement *self, GdomeDOMString *id, GdomeException *exc);
GdomeDOMString *gdome_HTMLElement_title (GdomeHTMLElement *self, GdomeException *exc);
void gdome_HTMLElement_set_title (GdomeHTMLElement *self, GdomeDOMString *title, GdomeException *exc);
GdomeDOMString *gdome_HTMLElement_lang (GdomeHTMLElement *self, GdomeException *exc);
void gdome_HTMLElement_set_lang (GdomeHTMLElement *self, GdomeDOMString *lang, GdomeException *exc);
GdomeDOMString *gdome_HTMLElement_dir (GdomeHTMLElement *self, GdomeException *exc);
void gdome_HTMLElement_set_dir (GdomeHTMLElement *self, GdomeDOMString *dir, GdomeException *exc);
GdomeDOMString *gdome_HTMLElement_className (GdomeHTMLElement *self, GdomeException *exc);
void gdome_HTMLElement_set_className (GdomeHTMLElement *self, GdomeDOMString *className, GdomeException *exc);
GdomeDOMString *gdome_HTMLElement_tagName (GdomeHTMLElement *self, GdomeException *exc);
GdomeDOMString *gdome_HTMLElement_getAttribute (GdomeHTMLElement *self, GdomeDOMString *name, GdomeException *exc);
void gdome_HTMLElement_setAttribute (GdomeHTMLElement *self, GdomeDOMString *name, GdomeDOMString *value, GdomeException *exc);
void gdome_HTMLElement_removeAttribute (GdomeHTMLElement *self, GdomeDOMString *name, GdomeException *exc);
GdomeAttr *gdome_HTMLElement_getAttributeNode (GdomeHTMLElement *self, GdomeDOMString *name, GdomeException *exc);
GdomeAttr *gdome_HTMLElement_setAttributeNode (GdomeHTMLElement *self, GdomeAttr *newAttr, GdomeException *exc);
GdomeAttr *gdome_HTMLElement_removeAttributeNode (GdomeHTMLElement *self, GdomeAttr *oldAttr, GdomeException *exc);
GdomeNodeList *gdome_HTMLElement_getElementsByTagName (GdomeHTMLElement *self, GdomeDOMString *name, GdomeException *exc);
GdomeDOMString *gdome_HTMLElement_getAttributeNS (GdomeHTMLElement *self, GdomeDOMString *namespaceURI, GdomeDOMString *localName, GdomeException *exc);
void gdome_HTMLElement_setAttributeNS (GdomeHTMLElement *self, GdomeDOMString *namespaceURI, GdomeDOMString *localName, GdomeDOMString *value, GdomeException *exc);
void gdome_HTMLElement_removeAttributeNS (GdomeHTMLElement *self, GdomeDOMString *namespaceURI, GdomeDOMString *localName, GdomeException *exc);
GdomeAttr *gdome_HTMLElement_getAttributeNodeNS (GdomeHTMLElement *self, GdomeDOMString *namespaceURI, GdomeDOMString *localName, GdomeException *exc);
GdomeAttr *gdome_HTMLElement_setAttributeNodeNS (GdomeHTMLElement *self, GdomeAttr *newAttr, GdomeException *exc);
GdomeNodeList *gdome_HTMLElement_getElementsByTagNameNS (GdomeHTMLElement *self, GdomeDOMString *namespaceURI, GdomeDOMString *localName, GdomeException *exc);
void gdome_HTMLElement_ref (GdomeHTMLElement *self, GdomeException *exc);
void gdome_HTMLElement_unref (GdomeHTMLElement *self, GdomeException *exc);
void * gdome_HTMLElement_query_interface (GdomeHTMLElement *self, const char *interface, GdomeException *exc);
GdomeDOMString *gdome_HTMLElement_nodeName (GdomeHTMLElement *self, GdomeException *exc);
GdomeDOMString *gdome_HTMLElement_nodeValue (GdomeHTMLElement *self, GdomeException *exc);
void gdome_HTMLElement_set_nodeValue (GdomeHTMLElement *self, GdomeDOMString *nodeValue, GdomeException *exc);
unsigned short gdome_HTMLElement_nodeType (GdomeHTMLElement *self, GdomeException *exc);
GdomeNode *gdome_HTMLElement_parentNode (GdomeHTMLElement *self, GdomeException *exc);
GdomeNodeList *gdome_HTMLElement_childNodes (GdomeHTMLElement *self, GdomeException *exc);
GdomeNode *gdome_HTMLElement_firstChild (GdomeHTMLElement *self, GdomeException *exc);
GdomeNode *gdome_HTMLElement_lastChild (GdomeHTMLElement *self, GdomeException *exc);
GdomeNode *gdome_HTMLElement_previousSibling (GdomeHTMLElement *self, GdomeException *exc);
GdomeNode *gdome_HTMLElement_nextSibling (GdomeHTMLElement *self, GdomeException *exc);
GdomeNamedNodeMap *gdome_HTMLElement_attributes (GdomeHTMLElement *self, GdomeException *exc);
GdomeDocument *gdome_HTMLElement_ownerDocument (GdomeHTMLElement *self, GdomeException *exc);
GdomeNode *gdome_HTMLElement_insertBefore (GdomeHTMLElement *self, GdomeNode *newChild, GdomeNode *refChild, GdomeException *exc);
GdomeNode *gdome_HTMLElement_replaceChild (GdomeHTMLElement *self, GdomeNode *newChild, GdomeNode *oldChild, GdomeException *exc);
GdomeNode *gdome_HTMLElement_removeChild (GdomeHTMLElement *self, GdomeNode *oldChild, GdomeException *exc);
GdomeNode *gdome_HTMLElement_appendChild (GdomeHTMLElement *self, GdomeNode *newChild, GdomeException *exc);
GdomeBoolean gdome_HTMLElement_hasChildNodes (GdomeHTMLElement *self, GdomeException *exc);
GdomeNode *gdome_HTMLElement_cloneNode (GdomeHTMLElement *self, GdomeBoolean deep, GdomeException *exc);
void gdome_HTMLElement_normalize (GdomeHTMLElement *self, GdomeException *exc);
GdomeBoolean gdome_HTMLElement_isSupported (GdomeHTMLElement *self, GdomeDOMString *feature, GdomeDOMString *version, GdomeException *exc);
GdomeDOMString *gdome_HTMLElement_namespaceURI (GdomeHTMLElement *self, GdomeException *exc);
GdomeDOMString *gdome_HTMLElement_prefix (GdomeHTMLElement *self, GdomeException *exc);
void gdome_HTMLElement_set_prefix (GdomeHTMLElement *self, GdomeDOMString *prefix, GdomeException *exc);
GdomeDOMString *gdome_HTMLElement_localName (GdomeHTMLElement *self, GdomeException *exc);
void gdome_HTMLElement_addEventListener (GdomeHTMLElement *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc);
void gdome_HTMLElement_removeEventListener (GdomeHTMLElement *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc);
GdomeBoolean gdome_HTMLElement_dispatchEvent (GdomeHTMLElement *self, GdomeEvent *evt, GdomeException *exc);

#endif /* GDOME_HTML_H */

