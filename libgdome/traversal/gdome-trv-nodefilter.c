/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*- */
/* gdome-trv-nodefilter.c
 *
 * Copyright (C) 2002 Paolo Casarini <paolo@casarini.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
#include <stdlib.h>
#include <string.h>
#include "gdome.h"
#include "gdome-traversal.h"
#include "gdome-xml-util.h"
#include "gdome-trv-nodefilter.h"

const GdomeNodeFilterVtab gdome_trv_nf_vtab =
{
  gdome_trv_nf_ref,
  gdome_trv_nf_unref,
  gdome_trv_nf_query_interface,
  gdome_trv_nf_acceptNode
};

/**
 * gdome_trv_nf_mkref:
 * @callback:  the callback function that is to be called when
 *             gdome_nf_acceptNode() is called.
 *
 * Returns: a new allocated #GdomeNodeFilter.
 */
GdomeNodeFilter *
gdome_trv_nf_mkref (short (*callback) (GdomeNodeFilter *self,
																			 GdomeNode *n,
																			 GdomeException *exc))
{
  Gdome_trv_NodeFilter *result = NULL;

  result = g_new (Gdome_trv_NodeFilter, 1);
#ifdef DEBUG_REFCNT
/*  gdome_refdbg_addRef ((void *)result, GDOME_REFDBG_EVENTLISTENER);*/
#endif
  result->refcnt = 1;
  result->vtab = (GdomeNodeFilterVtab *) &gdome_trv_nf_vtab;
  result->super.user_data = NULL;

  result->callback = callback;

  return (GdomeNodeFilter *)result;
}


/**
 * gdome_trv_nf_ref:
 * @self:  NodeFilter Object ref
 * @exc:  Exception Object ref
 *
 * Increase the reference count of the specified NodeFilter.
 */
void
gdome_trv_nf_ref (GdomeNodeFilter *self, GdomeException *exc)
{
  Gdome_trv_NodeFilter *priv = (Gdome_trv_NodeFilter *)self;

  g_return_if_fail (priv != NULL);
  g_return_if_fail (exc != NULL);

  priv->refcnt++;
}

/**
 * gdome_trv_nf_unref:
 * @self:  NodeFilter Object ref
 * @exc:  Exception Object ref
 *
 * Decrease the reference count of the specified NodeFilter. Free the
 * NodeFilter structure if the NodeFilter will have zero reference.
 */
void
gdome_trv_nf_unref (GdomeNodeFilter *self, GdomeException *exc)
{
  Gdome_trv_NodeFilter *priv = (Gdome_trv_NodeFilter *)self;

  g_return_if_fail (priv != NULL);
  g_return_if_fail (exc != NULL);

  if (--priv->refcnt == 0) {
#ifdef DEBUG_REFCNT
/*    gdome_refdbg_delRef ((void *)self, GDOME_REFDBG_EVENTLISTENER); */
#endif
		g_free (self);
  }

}

/**
 * gdome_trv_nf_query_interface:
 * @self:  NodeFilter Object ref
 * @interface:  name of the Interface needed.
 * @exc:  Exception Object ref
 *
 * Returns: a reference to the object that implements the interface needed or
 * %NULL if this object doesn't implement the interface specified.
 */
gpointer
gdome_trv_nf_query_interface (GdomeNodeFilter *self,
															const char *interface,
															GdomeException *exc)
{
	Gdome_trv_NodeFilter *priv = (Gdome_trv_NodeFilter *)self;

	g_return_val_if_fail (priv != NULL, NULL);
	g_return_val_if_fail (interface != NULL, NULL);
	g_return_val_if_fail (exc != NULL, NULL);

	if (!strcmp (interface, "NodeFilter")) {
		priv->refcnt++;

		return self;
	}
	else
    return NULL;
}

/**
 * gdome_trv_nf_acceptNode:
 * @self:  NodeFilter Object ref
 * @n:  The node to check to see if it passes the filter or not..
 * @exc:  Exception Object ref
 *
 * Test whether a specified node is visible in the logical view of a
 * #GdomeTreeWalker or #GdomeNodeIterator. This function will be called by the
 * implementation of #GdomeTreeWalker and #GdomeNodeIterator; it is not
 * normally called directly from the user code. (Though you could do so if you
 * wanted to use the same filter to guide your own application logic.)
 *
 * Returns: a costant to determine wheter the node is accepted, rejected or
 * skipped. In particular it returns %GDOME_FILTER_ACCEPT or
 * %GDOME_FILTER_REJECT or %GDOME_FILTER_SKIP.
 */
short
gdome_trv_nf_acceptNode (GdomeNodeFilter *self, GdomeNode *n, GdomeException *exc)
{
	Gdome_trv_NodeFilter *priv = (Gdome_trv_NodeFilter *)self;

  g_return_val_if_fail (priv != NULL, 0);
  g_return_val_if_fail (n != NULL, 0);
  g_return_val_if_fail (GDOME_XML_IS_N (n), 0);
  g_return_val_if_fail (exc != NULL, 0);

  return priv->callback (self, n, exc);
}
