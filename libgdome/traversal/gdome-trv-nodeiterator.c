/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*- */
/* gdome-trv-nodeiterator.c
 *
 * Copyright (C) 2002 Paolo Casarini <paolo@casarini.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
#include <stdlib.h>
#include <string.h>
#include "gdome.h"
#include "gdome-traversal.h"
#include "gdome-xml-node.h"
#include "gdome-trv-nodefilter.h"
#include "gdome-trv-nodeiterator.h"

const GdomeNodeIteratorVtab gdome_trv_ni_vtab =
{
  gdome_trv_ni_ref,
  gdome_trv_ni_unref,
  gdome_trv_ni_query_interface,
  gdome_trv_ni_root,
  gdome_trv_ni_whatToShow,
  gdome_trv_ni_filter,
  gdome_trv_ni_expandEntityReference,
  gdome_trv_ni_nextNode,
  gdome_trv_ni_previousNode,
  gdome_trv_ni_detach
};

xmlNode *gdome_trvGetNext (xmlNode *root, xmlNode *ref);
xmlNode *gdome_trvGetPrevious (xmlNode *root, xmlNode *ref);

/**
 * gdome_trv_ni_mkref:
 * @root:  the root node for thid NodeIterator
 * @whatToShow:  flags to filter nodes on its type
 * @filter:  NodeFilter Object ref to apply on this NodeIterator
 *
 * Returns: a new allocated #GdomeNodeIterator.
 */
GdomeNodeIterator *
gdome_trv_ni_mkref (GdomeNode *root,
										guint32 whatToShow,
										GdomeNodeFilter *filter,
										GdomeBoolean entityReferenceExpansion)
{
	Gdome_trv_NodeIterator *result = NULL;
	GdomeException exc;

	result = g_new (Gdome_trv_NodeIterator, 1);
	memset(result, 0, sizeof(Gdome_trv_NodeIterator));
#ifdef DEBUG_REFCNT
/*  gdome_refdbg_addRef ((void *)result, GDOME_REFDBG_);*/
#endif

	result->vtab = &gdome_trv_ni_vtab;
	result->refcnt = 1;
	gdome_xml_n_ref (root, &exc);
	result->root = root;
	result->whatToShow = whatToShow;
	if(filter != NULL) {
		gdome_trv_nf_ref (filter, &exc);
		result->filter = filter;
	}

	return (GdomeNodeIterator *)result;
}

/**
 * gdome_trv_ni_ref:
 * @self:  NodeIterator Object ref
 * @exc:  Exception Object ref
 *
 * Increase the reference count of the specified NodeIterator.
 */
void
gdome_trv_ni_ref (GdomeNodeIterator *self, GdomeException *exc)
{
  Gdome_trv_NodeIterator *priv = (Gdome_trv_NodeIterator *)self;

  g_return_if_fail (priv != NULL);
  g_return_if_fail (exc != NULL);

  priv->refcnt++;
}

/**
 * gdome_trv_ni_unref:
 * @self:  NodeIterator Object ref
 * @exc:  Exception Object ref
 *
 * Decrease the reference count of the specified NodeIterator. Free the
 * NodeIterator structure if the NodeIterator will have zero reference.
 */
void
gdome_trv_ni_unref (GdomeNodeIterator *self, GdomeException *exc)
{
  Gdome_trv_NodeIterator *priv = (Gdome_trv_NodeIterator *)self;
	
  g_return_if_fail (priv != NULL);
  g_return_if_fail (exc != NULL);

	if (--priv->refcnt == 0) {
    if (priv->root != NULL)
			gdome_xml_n_unref (priv->root, exc);
    if (priv->reference != NULL)
			gdome_xml_n_unref (priv->reference, exc);
    if (priv->filter != NULL)
			gdome_trv_nf_unref (priv->filter, exc);
#ifdef DEBUG_REFCNT
/*    gdome_refdbg_delRef ((void *)self, GDOME_REFDBG_);*/
#endif
		g_free (self);
	}
}

/**
 * gdome_trv_ni_query_interface:
 * @self:  NodeIterator Object ref
 * @interface:  name of the Interface needed.
 * @exc:  Exception Object ref
 *
 * Returns: a reference to the object that implements the interface needed or
 * %NULL if this object doesn't implement the interface specified.
 */
gpointer
gdome_trv_ni_query_interface (GdomeNodeIterator *self,
															const char *interface,
															GdomeException *exc)
{
	Gdome_trv_NodeIterator *priv = (Gdome_trv_NodeIterator *)self;

	g_return_val_if_fail (priv != NULL, NULL);
	g_return_val_if_fail (interface != NULL, NULL);
	g_return_val_if_fail (exc != NULL, NULL);

	if (!strcmp (interface, "NodeIterator")) {
		priv->refcnt++;
		return self;
	}
	else
		return NULL;
}

/**
 * gdome_trv_ni_root:
 * @self: NodeIterator Object ref
 * @exc:  Exception Object ref
 *
 * Returns: The root node of the NodeIterator, as specified when it was created
 */
GdomeNode *
gdome_trv_ni_root (GdomeNodeIterator *self, GdomeException *exc)
{
	Gdome_trv_NodeIterator *priv = (Gdome_trv_NodeIterator *)self;
	
  g_return_val_if_fail (priv != NULL, NULL);
  g_return_val_if_fail (exc != NULL, NULL);

  gdome_xml_n_ref (priv->root, exc);
  return priv->root;
}

/**
 * gdome_trv_ni_whatToShow:
 * @self: NodeIterator Object ref
 * @exc:  Exception Object ref
 *
 * Returns: This attribute determines which node types are presented via the
 * iterator. The available set of costants is defined in #GdomeWhatToShowType.
 * Nodes not accepted by @whatToShow will be skipped, baut their children may
 * still be considered. Note that this skip takes precedence over the filter,
 * if any.
 */
guint32
gdome_trv_ni_whatToShow (GdomeNodeIterator *self, GdomeException *exc)
{
  Gdome_trv_NodeIterator *priv = (Gdome_trv_NodeIterator *)self;
	
  g_return_val_if_fail (priv != NULL, 0);
  g_return_val_if_fail (exc != NULL, 0);

  return priv->whatToShow;
}

/**
 * gdome_trv_ni_filter:
 * @self: NodeIterator Object ref
 * @exc:  Exception Object ref
 *
 * Returns: The #GdomeNodeFilter used to screen nodes.
 */
GdomeNodeFilter *
gdome_trv_ni_filter (GdomeNodeIterator *self, GdomeException *exc)
{
	Gdome_trv_NodeIterator *priv = (Gdome_trv_NodeIterator *)self;
	
  g_return_val_if_fail (priv != NULL, NULL);
  g_return_val_if_fail (exc != NULL, NULL);

  gdome_trv_nf_ref (priv->filter, exc);
  return priv->filter;
}

/**
 * gdome_trv_ni_expandEntityReference:
 * @self: NodeIterator Object ref
 * @exc:  Exception Object ref
 *
 * Returns: %TRUE if the entityReferenceExpansion flag is set, %FALSE
 * otherwise.
 */
GdomeBoolean
gdome_trv_ni_expandEntityReference (GdomeNodeIterator *self, GdomeException *exc)
{
  Gdome_trv_NodeIterator *priv = (Gdome_trv_NodeIterator *)self;
	
  g_return_val_if_fail (priv != NULL, FALSE);
  g_return_val_if_fail (exc != NULL, FALSE);

  return priv->entityReferenceExpansion;
}

/**
 * gdome_trv_ni_nextNode:
 * @self: NodeIterator Object ref
 * @exc:  Exception Object ref
 *
 * Returns the next node in the set and advances the position of the
 * iterator in the set. After a #GdomeNodeIterator is created, the first
 * call to gdome_ni_nextNode() returns the first node in the set.
 *
 * %GDOME_INVALID_STATE_ERR: Raised if this method is called after the
 * gdome_ni_detach() method was invoked.
 * Returns: the next #GdomeNode in the set being iterated over, or %NULL if
 * there are no more members in that set.
 */
GdomeNode *
gdome_trv_ni_nextNode (GdomeNodeIterator *self, GdomeException *exc)
{
	Gdome_trv_NodeIterator *priv = (Gdome_trv_NodeIterator *)self;
  xmlNode *res = NULL;
	
  g_return_val_if_fail (priv != NULL, NULL);
	g_return_val_if_fail (exc != NULL, NULL);

	if (priv->root == NULL) {
		*exc = GDOME_INVALID_STATE_ERR;
		return NULL;
  }

  if (priv->reference == NULL)
		res = gdome_trvGetNext (((Gdome_xml_Node *)priv->root)->n, NULL);
  else
		res = gdome_trvGetNext (((Gdome_xml_Node *)priv->root)->n,
														((Gdome_xml_Node *)priv->reference)->n);

	if (res != NULL) {
		priv->reference = gdome_xml_n_mkref (res);
		return priv->reference;
	}
	else
		return NULL;
}

/**
 * gdome_trv_ni_previousNode:
 * @self: NodeIterator Object ref
 * @exc:  Exception Object ref
 *
 * Returns the previous node in the set and moves the position of the
 * #GdomeNodeIterator backwards in the set.
 *
 * %GDOME_INVALID_STATE_ERR: Raised if this methid is called after the
 * gdome_ni_detach() method was invoked.
 * Returns: the previous #GdomeNode in the set being iterated over, or %NULL
 * if there are no more members in that set.
 */
GdomeNode *
gdome_trv_ni_previousNode (GdomeNodeIterator *self, GdomeException *exc)
{
	Gdome_trv_NodeIterator *priv = (Gdome_trv_NodeIterator *)self;
  xmlNode *res = NULL;
	
  g_return_val_if_fail (priv != NULL, NULL);
	g_return_val_if_fail (exc != NULL, NULL);

	if (priv->root == NULL) {
		*exc = GDOME_INVALID_STATE_ERR;
		return NULL;
	}

	if (priv->reference != NULL)
		res = gdome_trvGetNext (((Gdome_xml_Node *)priv->root)->n,
														((Gdome_xml_Node *)priv->reference)->n);

	if (res != NULL) {
		priv->reference = gdome_xml_n_mkref (res);
		return priv->reference;
	}
	else
		return NULL;
}

/**
 * gdome_trv_ni_detach:
 * @self: NodeIterator Object ref
 * @exc:  Exception Object ref
 *
 * Detaches the #GdomeNodeIterator from the set which it iterated over,
 * releasing any computational resourses and placing the iterator in
 * INVALID state. After gdome_ni_detach() has been invoked, calls to
 * gdome_ni_nextNode() and gdome_ni_previousNode() will raise the exception
 * %GDOME_INVALID_STATE_ERR.
 */
void
gdome_trv_ni_detach (GdomeNodeIterator *self, GdomeException *exc)
{
  Gdome_trv_NodeIterator *priv = (Gdome_trv_NodeIterator *)self;
	
  g_return_if_fail (priv != NULL);
	g_return_if_fail (exc != NULL);

	if (priv->root != NULL) {
		gdome_xml_n_unref (priv->root, exc);
		priv->root = NULL;
	}
	if (priv->reference != NULL) {
		gdome_xml_n_unref (priv->reference, exc);
		priv->reference = NULL;
	}
	if (priv->filter != NULL) {
		gdome_trv_nf_unref (priv->filter, exc);
		priv->filter = NULL;
	}
}

xmlNode *
gdome_trvGetNext (xmlNode *root, xmlNode *ref)
{
	if (ref == NULL)
		return root;

	if (ref->children != NULL)
		return ref->children;
	else if (ref->next != NULL)
		return ref->next;
	else {
		while (ref->parent != root->last && ref->parent->next == NULL)
			ref = ref->parent;
		if (ref->parent == root->last)
			return NULL;
    else
			return ref->parent->next;
	}
}

xmlNode *
gdome_trvGetPreviuous (xmlNode *root, xmlNode *ref)
{
	if (ref == NULL)
		return NULL;

	if (ref->prev != NULL) {
		if (ref->prev->last == NULL)
			return ref->prev;
		else {
			ref = ref->prev;
			while (ref->last != NULL)
				ref = ref->last;
			return ref;
		}
	}
	else if (ref->parent != NULL)
		return ref->parent;
	else
		return NULL;
}
