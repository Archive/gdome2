/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*- */
/* gdome-trv-nodeiterator.h
 *
 * Copyright (C) 2002 Paolo Casarini <paolo@casarini.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


/* --------------------------------------------------------------- */
/* ------------------------  NodeIterator  ----------------------- */
/* --------------------------------------------------------------- */


#ifndef GDOME_NODEITERATOR_FILE
#define GDOME_NODEITERATOR_FILE

struct _GdomeNodeIteratorVtab {
  void (*ref) (GdomeNodeIterator *self, GdomeException *exc);
  void (*unref) (GdomeNodeIterator *self, GdomeException *exc);
  gpointer (*query_interface) (GdomeNodeIterator *self, const char *interface, GdomeException *exc);
  GdomeNode * (*root) (GdomeNodeIterator *self, GdomeException *exc);
  guint32 (*whatToShow) (GdomeNodeIterator *self, GdomeException *exc);
  GdomeNodeFilter * (*filter) (GdomeNodeIterator *self, GdomeException *exc);
  GdomeBoolean (*expandEntityReference) (GdomeNodeIterator *self, GdomeException *exc);
  GdomeNode * (*nextNode) (GdomeNodeIterator *self, GdomeException *exc);
  GdomeNode * (*previousNode) (GdomeNodeIterator *self, GdomeException *exc);
  void (*detach) (GdomeNodeIterator *self, GdomeException *exc);
};

typedef struct _Gdome_trv_NodeIterator Gdome_trv_NodeIterator;
struct _Gdome_trv_NodeIterator {
  GdomeNodeIterator super;
	const GdomeNodeIteratorVtab *vtab;
	int refcnt;
  GdomeNode *root;
  GdomeNode *reference;
  guint32 whatToShow;
  GdomeNodeFilter *filter;
};

GdomeNodeIterator *gdome_trv_ni_mkref                 (GdomeNode *root,
                                                       guint32 whatToShow,
                                                       GdomeNodeFilter *filter,
                                                       GdomeBoolean entityReferenceExpansion);
void               gdome_trv_ni_ref                   (GdomeNodeIterator *self,
                                                       GdomeException *exc);
void               gdome_trv_ni_unref                 (GdomeNodeIterator *self,
                                                       GdomeException *exc);
gpointer           gdome_trv_ni_query_interface       (GdomeNodeIterator *self,
                                                       const char *interface,
                                                       GdomeException *exc);
GdomeNode *        gdome_trv_ni_root                  (GdomeNodeIterator *self,
                                                       GdomeException *exc);
guint32            gdome_trv_ni_whatToShow            (GdomeNodeIterator *self,
                                                       GdomeException *exc);
GdomeNodeFilter *  gdome_trv_ni_filter                (GdomeNodeIterator *self,
                                                       GdomeException *exc);
GdomeBoolean       gdome_trv_ni_expandEntityReference (GdomeNodeIterator *self,
                                                       GdomeException *exc);
GdomeNode *        gdome_trv_ni_nextNode              (GdomeNodeIterator *self,
                                                       GdomeException *exc);
GdomeNode *        gdome_trv_ni_previousNode          (GdomeNodeIterator *self,
                                                       GdomeException *exc);
void               gdome_trv_ni_detach                (GdomeNodeIterator *self,
                                                       GdomeException *exc);

extern const GdomeNodeIteratorVtab gdome_trv_ni_vtab;

#endif /* GDOME_NODEITERATOR_FILE */
