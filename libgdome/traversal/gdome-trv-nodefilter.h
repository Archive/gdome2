/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*- */
/* gdome-trv-nodefilter.h
 *
 * Copyright (C) 2002 Paolo Casarini <paolo@casarini.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


/* --------------------------------------------------------------- */
/* -------------------------  NodeFilter  ------------------------ */
/* --------------------------------------------------------------- */


#ifndef GDOME_NODEFILTER_FILE
#define GDOME_NODEFILTER_FILE

struct _GdomeNodeFilterVtab {
  void (*ref) (GdomeNodeFilter *self, GdomeException *exc);
  void (*unref) (GdomeNodeFilter *self, GdomeException *exc);
  gpointer (*query_interface) (GdomeNodeFilter *self, const char *interface, GdomeException *exc);
  short (*acceptNode) (GdomeNodeFilter *self, GdomeNode *n, GdomeException *exc);
};

typedef struct _Gdome_trv_NodeFilter Gdome_trv_NodeFilter;

struct _Gdome_trv_NodeFilter {
  GdomeNodeFilter super;
  const GdomeNodeFilterVtab *vtab;
  int refcnt;
	short (*callback) (GdomeNodeFilter *self,
                    GdomeNode *n,
                    GdomeException *exc);
};

GdomeNodeFilter *gdome_trv_nf_mkref           (short (*callback) (GdomeNodeFilter *self,
                                                                  GdomeNode *n,
                                                                  GdomeException *exc));
void             gdome_trv_nf_ref             (GdomeNodeFilter *self,
                                               GdomeException *exc);
void             gdome_trv_nf_unref           (GdomeNodeFilter *self,
                                               GdomeException *exc);
gpointer         gdome_trv_nf_query_interface (GdomeNodeFilter *self,
                                               const char *interface,
                                               GdomeException *exc);
short            gdome_trv_nf_acceptNode      (GdomeNodeFilter *self,
                                               GdomeNode *n,
                                               GdomeException *exc);



extern const GdomeNodeFilterVtab gdome_trv_nf_vtab;

#endif /* GDOME_NODEFILTER_FILE */




