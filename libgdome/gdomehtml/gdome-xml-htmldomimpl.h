/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*- */
/* gdome-xml-htmldomimpl.c
 *
 * Copyright (C) 1999 Raph Levien <raph@acm.org>
 * Copyright (C) 2000 Mathieu Lacage <mathieu@gnu.org>
 * Copyright (C) 2000 Anders Carlsson <andersca@gnu.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef GDOME_XML_HTMLDOMIMPL_H
#define GDOME_XML_HTMLDOMIMPL_H

#include <glib.h>
#include "gdome.h"
#include "gdome-html.h"

typedef struct _Gdome_xml_HTMLDOMImplementation Gdome_xml_HTMLDOMImplementation;

struct _Gdome_xml_HTMLDOMImplementation {
	GdomeHTMLDOMImplementation super;
	int refcnt;
};

const GdomeHTMLDOMImplementationVtab gdome_xml_HTMLDOMImplementation_vtab;

void           gdome_xml_HTMLDOMImplementation_ref (GdomeDOMImplementation *self,
																										GdomeException *exc);

void           gdome_xml_HTMLDOMImplementation_unref (GdomeDOMImplementation *self,
																											GdomeException *exc);

void         * gdome_xml_HTMLDOMImplementation_query_interface (GdomeDOMImplementation *self,
																																const char *interface, GdomeException *exc);

GdomeHTMLDocument * gdome_xml_HTMLDOMImplementation_createHTMLDocument (GdomeHTMLDOMImplementation *self,
																																				GdomeDOMString *title, GdomeException *exc);
#endif /* GDOME_XML_HTMLDOMIMP_H */
