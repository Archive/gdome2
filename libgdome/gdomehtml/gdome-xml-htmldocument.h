/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*- */
/* gdome-xml-htmldomimpl.c
 *
 * Copyright (C) 1999 Raph Levien <raph@acm.org>
 * Copyright (C) 2000 Mathieu Lacage <mathieu@gnu.org>
 * Copyright (C) 2000 Anders Carlsson <andersca@gnu.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef GDOME_XML_HTMLDOCUMENT_H
#define GDOME_XML_HTMLDOCUMENT_H

#include <glib.h>
#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xmlmemory.h>
#include "gdome.h"
#include "gdome-html.h"

typedef struct _Gdome_xml_HTMLDocument Gdome_xml_HTMLDocument;

GdomeDOMString *gdome_xml_HTMLDocument_title (GdomeHTMLDocument *self, GdomeException *exc);
void gdome_xml_HTMLDocument_set_title (GdomeHTMLDocument *self, GdomeDOMString *title, GdomeException *exc);
GdomeDOMString *gdome_xml_HTMLDocument_referrer (GdomeHTMLDocument *self, GdomeException *exc);
GdomeDOMString *gdome_xml_HTMLDocument_domain (GdomeHTMLDocument *self, GdomeException *exc);
GdomeDOMString *gdome_xml_HTMLDocument_URL (GdomeHTMLDocument *self, GdomeException *exc);
GdomeHTMLElement *gdome_xml_HTMLDocument_body (GdomeHTMLDocument *self, GdomeException *exc);
void gdome_HTMLDocument_set_body (GdomeHTMLDocument *self, GdomeHTMLElement *body, GdomeException *exc);
GdomeHTMLCollection *gdome_xml_HTMLDocument_images (GdomeHTMLDocument *self, GdomeException *exc);
GdomeHTMLCollection *gdome_xml_HTMLDocument_applets (GdomeHTMLDocument *self, GdomeException *exc);
GdomeHTMLCollection *gdome_xml_HTMLDocument_links (GdomeHTMLDocument *self, GdomeException *exc);
GdomeHTMLCollection *gdome_xml_HTMLDocument_forms (GdomeHTMLDocument *self, GdomeException *exc);
GdomeHTMLCollection *gdome_xml_HTMLDocument_anchors (GdomeHTMLDocument *self, GdomeException *exc);
GdomeDOMString *gdome_xml_HTMLDocument_cookie (GdomeHTMLDocument *self, GdomeException *exc);
void gdome_xml_HTMLDocument_set_cookie (GdomeHTMLDocument *self, GdomeDOMString *cookie, GdomeException *exc);
void gdome_xml_HTMLDocument_open (GdomeHTMLDocument *self, GdomeException *exc);
void gdome_xml_HTMLDocument_close (GdomeHTMLDocument *self, GdomeException *exc);
void gdome_xml_HTMLDocument_write (GdomeHTMLDocument *self, GdomeDOMString *text, GdomeException *exc);
void gdome_xml_HTMLDocument_writeln (GdomeHTMLDocument *self, GdomeDOMString *text, GdomeException *exc);
GdomeNodeList *gdome_xml_HTMLDocument_getElementsByName (GdomeHTMLDocument *self, GdomeDOMString *elementName, GdomeException *exc);

#endif /* GDOME_XML_HTMLDOCUMENT_H */
