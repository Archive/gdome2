/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*- */
/* gdome-xml-htmlelement.c
 *
 * Copyright (C) 1999 Raph Levien <raph@acm.org>
 * Copyright (C) 2000 Mathieu Lacage <mathieu@gnu.org>
 * Copyright (C) 2000 Anders Carlsson <andersca@gnu.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <glib.h>
#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xmlmemory.h>
#include "gdome.h"
#include "gdome-html.h"
#include "gdome-xml-str.h"
#include "gdome-xml-node.h"
#include "gdome-xml-element.h"
#include "gdome-xml-htmlelement.h"

const GdomeHTMLElementVtab gdome_xml_HTMLElement_vtab = {
  {
	  {
			gdome_xml_n_ref,
			gdome_xml_n_unref,
			gdome_xml_n_query_interface,
			gdome_xml_n_nodeName,
			gdome_xml_n_nodeValue,
			gdome_xml_n_set_nodeValue,
			gdome_xml_n_nodeType,
			gdome_xml_n_parentNode,
			gdome_xml_n_childNodes,
			gdome_xml_n_firstChild,
			gdome_xml_n_lastChild,
			gdome_xml_n_previousSibling,
			gdome_xml_n_nextSibling,
			gdome_xml_el_attributes,
			gdome_xml_n_ownerDocument,
			gdome_xml_n_insertBefore,
			gdome_xml_n_replaceChild,
			gdome_xml_n_removeChild,
			gdome_xml_n_appendChild,
			gdome_xml_n_hasChildNodes,
			gdome_xml_n_cloneNode,
			gdome_xml_n_normalize,
			gdome_xml_n_supports,
			gdome_xml_n_namespaceURI,
			gdome_xml_n_prefix,
			gdome_xml_n_set_prefix,
			gdome_xml_n_localName,
			gdome_xml_n_addEventListener,
			gdome_xml_n_removeEventListener,
			gdome_xml_n_dispatchEvent
		},
		gdome_xml_el_tagName,
		gdome_xml_el_getAttribute,
		gdome_xml_el_setAttribute,
		gdome_xml_el_removeAttribute,
		gdome_xml_el_getAttributeNode,
		gdome_xml_el_setAttributeNode,
		gdome_xml_el_removeAttributeNode,
		gdome_xml_el_getElementsByTagName,
		gdome_xml_el_getAttributeNS,
		gdome_xml_el_setAttributeNS,
		gdome_xml_el_removeAttributeNS,
		gdome_xml_el_getAttributeNodeNS,
		gdome_xml_el_setAttributeNodeNS,
		gdome_xml_el_getElementsByTagNameNS,
		gdome_xml_el_hasAttribute,
		gdome_xml_el_hasAttributeNs
	},
	gdome_xml_HTMLElement_id,
	gdome_xml_HTMLElement_set_id,
	gdome_xml_HTMLElement_title,
	gdome_xml_HTMLElement_set_title,
	gdome_xml_HTMLElement_lang,
	gdome_xml_HTMLElement_set_lang,
	gdome_xml_HTMLElement_dir,
	gdome_xml_HTMLElement_set_dir,
	gdome_xml_HTMLElement_className,
	gdome_xml_HTMLElement_set_className
};

GdomeDOMString *
gdome_xml_HTMLElement_id (GdomeHTMLElement *self, GdomeException *exc)
{
	GdomeDOMString *str;
	GdomeDOMString *retval;
	
	str = gdome_xml_str_mkref ("id");
	retval = gdome_xml_el_getAttribute ((GdomeElement *)self, str, exc);
	gdome_str_unref (str);

	return retval;
}

void
gdome_xml_HTMLElement_set_id (GdomeHTMLElement *self, GdomeDOMString *id, GdomeException *exc)
{
	GdomeDOMString *str;
	
	str = gdome_xml_str_mkref ("id");
	gdome_xml_el_setAttribute ((GdomeElement *)self, str, id, exc);
	gdome_str_unref (str);
}

GdomeDOMString *
gdome_xml_HTMLElement_title (GdomeHTMLElement *self, GdomeException *exc)
{
	GdomeDOMString *str;
	GdomeDOMString *retval;
	
	str = gdome_xml_str_mkref ("title");
	retval = gdome_xml_el_getAttribute ((GdomeElement *)self, str, exc);
	gdome_str_unref (str);
	
	return retval;
}

void gdome_xml_HTMLElement_set_title (GdomeHTMLElement *self, GdomeDOMString *title, GdomeException *exc)
{
	GdomeDOMString *str;
	
	str = gdome_xml_str_mkref ("title");
	gdome_xml_el_setAttribute ((GdomeElement *)self, str, title, exc);
	gdome_str_unref (str);
}

GdomeDOMString *gdome_xml_HTMLElement_lang (GdomeHTMLElement *self, GdomeException *exc)
{
	GdomeDOMString *str;
	GdomeDOMString *retval;
	
	str = gdome_xml_str_mkref ("lang");
	retval = gdome_xml_el_getAttribute ((GdomeElement *)self, str, exc);
	gdome_str_unref (str);
	
	return retval;
}

void gdome_xml_HTMLElement_set_lang (GdomeHTMLElement *self, GdomeDOMString *lang, GdomeException *exc)
{
	GdomeDOMString *str;
	
	str = gdome_xml_str_mkref ("lang");
	gdome_xml_el_setAttribute ((GdomeElement *)self, str, lang, exc);
	gdome_str_unref (str);
}

GdomeDOMString *gdome_xml_HTMLElement_dir (GdomeHTMLElement *self, GdomeException *exc)
{
	GdomeDOMString *str;
	GdomeDOMString *retval;
	
	str = gdome_xml_str_mkref ("dir");
	retval = gdome_xml_el_getAttribute ((GdomeElement *)self, str, exc);
	gdome_str_unref (str);
	
	return retval;
}

void gdome_xml_HTMLElement_set_dir (GdomeHTMLElement *self, GdomeDOMString *dir, GdomeException *exc)
{
	GdomeDOMString *str;
	
	str = gdome_xml_str_mkref ("dir");
	gdome_xml_el_setAttribute ((GdomeElement *)self, str, dir, exc);
	gdome_str_unref (str);
}

GdomeDOMString *gdome_xml_HTMLElement_className (GdomeHTMLElement *self, GdomeException *exc)
{
	GdomeDOMString *str;
	GdomeDOMString *retval;
	
	str = gdome_xml_str_mkref ("class");
	retval = gdome_xml_el_getAttribute ((GdomeElement *)self, str, exc);
	gdome_str_unref (str);
	
	return retval;
}

void gdome_xml_HTMLElement_set_className (GdomeHTMLElement *self, GdomeDOMString *className, GdomeException *exc)
{
	GdomeDOMString *str;
	
	str = gdome_xml_str_mkref ("class");
	gdome_xml_el_setAttribute ((GdomeElement *)self, str, className, exc);
	gdome_str_unref (str);
}




