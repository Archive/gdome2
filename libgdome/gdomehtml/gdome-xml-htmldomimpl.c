/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*- */
/* gdome-xml-domimpl.c
 *
 * Copyright (C) 1999 Raph Levien <raph@acm.org>
 * Copyright (C) 2000 Mathieu Lacage <mathieu@gnu.org>
 * Copyright (C) 2000 Anders Carlsson <andersca@gnu.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include "gdome.h"
#include "gdome-html.h"
#include "gdome-xml-domimpl.h"
#include "gdome-xml-htmldomimpl.h"

Gdome_xml_HTMLDOMImplementation *gdome_xml_HTMLDOMImplementation = NULL;

/* vtable for the HTMLDOMImplementation */
const GdomeHTMLDOMImplementationVtab gdome_xml_HTMLDOMImplementation_vtab = {
	{
		gdome_xml_HTMLDOMImplementation_ref,
		gdome_xml_HTMLDOMImplementation_unref,
		gdome_xml_HTMLDOMImplementation_query_interface,
		gdome_xml_DOMImplementation_hasFeature,
		gdome_xml_DOMImplementation_createDocumentType,
		gdome_xml_DOMImplementation_createDocument
	},
	gdome_xml_HTMLDOMImplementation_createHTMLDocument
};

void
gdome_xml_HTMLDOMImplementation_ref (GdomeDOMImplementation *self,
																		 GdomeException *exc)
{
	Gdome_xml_HTMLDOMImplementation *priv = (Gdome_xml_HTMLDOMImplementation *)self;
	
	if (self == NULL)
		return;

	priv->refcnt++;
}

void
gdome_xml_HTMLDOMImplementation_unref (GdomeDOMImplementation *self,
																			 GdomeException *exc)
{
	Gdome_xml_HTMLDOMImplementation *priv = (Gdome_xml_HTMLDOMImplementation *)self;
	
	if (self == NULL)
		return;

	priv->refcnt--;

	if (priv->refcnt == 0)
		g_free (self);
}

void *
gdome_xml_HTMLDOMImplementation_query_interface (GdomeDOMImplementation *self, const char *interface, GdomeException *exc)
{
	return NULL;
}
	
GdomeHTMLDocument *
gdome_xml_HTMLDOMImplementation_createHTMLDocument (GdomeHTMLDOMImplementation *self,
																																				GdomeDOMString *title, GdomeException *exc)
{
	#warning implement HTMLDOMImplementation_createHTMLDocument
}



