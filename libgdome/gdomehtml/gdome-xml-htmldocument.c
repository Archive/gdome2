/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*- */
/* gdome-xml-htmldocument.c
 *
 * Copyright (C) 1999 Raph Levien <raph@acm.org>
 * Copyright (C) 2000 Mathieu Lacage <mathieu@gnu.org>
 * Copyright (C) 2000 Anders Carlsson <andersca@gnu.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <string.h>
#include "gdome-xml-htmldocument.h"
#include "gdome-xml-htmlcollection.h"
#include "gdome-xml-document.h"
#include "gdome-xml-nodel.h"

const GdomeHTMLDocumentVtab gdome_xml_HTMLDocument_vtab = {
  {
		{
			gdome_xml_n_ref,
			gdome_xml_n_unref,
			gdome_xml_n_query_interface,
			gdome_xml_n_nodeName,
			gdome_xml_n_nodeValue,
			gdome_xml_n_set_nodeValue,
			gdome_xml_n_nodeType,
			gdome_xml_n_parentNode,
			gdome_xml_n_childNodes,
			gdome_xml_n_firstChild,
			gdome_xml_n_lastChild,
			gdome_xml_n_previousSibling,
			gdome_xml_n_nextSibling,
			gdome_xml_n_attributes,
			gdome_xml_n_ownerDocument,
			gdome_xml_n_insertBefore,
			gdome_xml_n_replaceChild,
			gdome_xml_n_removeChild,
			gdome_xml_n_appendChild,
			gdome_xml_n_hasChildNodes,
			gdome_xml_n_cloneNode,
			gdome_xml_n_normalize,
			gdome_xml_n_isSupported,
			gdome_xml_n_namespaceURI,
			gdome_xml_n_prefix,
			gdome_xml_n_set_prefix,
			gdome_xml_n_localName,
			gdome_xml_n_addEventListener,
			gdome_xml_n_removeEventListener,
			gdome_xml_n_dispatchEvent
		},
		gdome_xml_doc_doctype,
		gdome_xml_doc_implementation,
		gdome_xml_doc_documentElement,
		gdome_xml_doc_createElement,
		gdome_xml_doc_createDocumentFragment,
		gdome_xml_doc_createTextNode,
		gdome_xml_doc_createComment,
		gdome_xml_doc_createCDATASection,
		gdome_xml_doc_createProcessingInstruction,
		gdome_xml_doc_createAttribute,
		gdome_xml_doc_createEntityReference,
		gdome_xml_doc_getElementsByTagName,
  },
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
	gdome_xml_HTMLDocument_images,
	gdome_xml_HTMLDocument_applets,
	gdome_xml_HTMLDocument_links,
	gdome_xml_HTMLDocument_forms,
	gdome_xml_HTMLDocument_anchors,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL,
  NULL
};

struct _Gdome_xml_HTMLDocument {
  GdomeHTMLDocument super;
};

static void
gdome_xml_HTMLDocument_traversal_helper (xmlNode *node, GdomeNodeList *nodelist, const char *str)
{
	xmlNode *n;

	n = node->children;

	while (n) {
		if (n->type == XML_ELEMENT_NODE) {
			if (strcmp (str, n->name) == 0 ||
					strcmp (str, "*") == 0) {
				/* We have a match */
				gdome_xml_nl_append (nodelist, gdome_xml_n_mkref (n));
			}
		}
		gdome_xml_HTMLDocument_traversal_helper (n, nodelist, str);
		n = n->next;
	}
}

static void
links_collection_traversal_helper (xmlNode *node, GdomeHTMLCollection *coll)
{
	xmlNode *n;

	n = node->children;

	while (n) {
		if (n->type == XML_ELEMENT_NODE) {
			if (strcmp (n->name, "a") == 0 ||
					strcmp (n->name, "area") == 0) {
				char *prop = xmlGetProp (node, "href");
				
				if (prop != NULL)
					gdome_xml_HTMLCollection_append (coll, gdome_xml_n_mkref (n));

				xmlFree (prop);
			}
		}
		links_collection_traversal_helper (n, coll);
		n = n->next;
	}
}

GdomeHTMLCollection *
gdome_xml_HTMLDocument_links (GdomeHTMLDocument *self, GdomeException *exc)
{
	Gdome_xml_Node *priv = (Gdome_xml_Node *)self;
	GdomeHTMLCollection *coll;
	xmlDoc *doc = NULL;
	
	if (self == NULL)
		return NULL;
	
	doc = (xmlDoc *)priv->n;
	
	if (doc == NULL)
		return NULL;
	if (doc->children == NULL)
		return NULL;
	
	coll = gdome_xml_HTMLCollection_mkref ();
	
	links_collection_traversal_helper (doc->children, coll);

	return coll;
}

static void
forms_collection_traversal_helper (xmlNode *node, GdomeHTMLCollection *coll)
{
	xmlNode *n;
	
	n = node->children;
	
	while (n) {
		if (n->type == XML_ELEMENT_NODE) {
			if (strcmp ("form", n->name) == 0) {
				/* We have a match */
				gdome_xml_HTMLCollection_append (coll, gdome_xml_n_mkref (n));
			}
		}
		
		forms_collection_traversal_helper (n, coll);
		
	}
	
	n = n->next;
}

GdomeHTMLCollection *
gdome_xml_HTMLDocument_forms (GdomeHTMLDocument *self, GdomeException *exc)
{
	Gdome_xml_Node *priv = (Gdome_xml_Node *)self;
	GdomeHTMLCollection *coll;
	xmlDoc *doc = NULL;
	
	if (self == NULL)
		return NULL;
	
	doc = (xmlDoc *)priv->n;
	
	if (doc == NULL)
		return NULL;
	if (doc->children == NULL)
		return NULL;
	
	coll = gdome_xml_HTMLCollection_mkref ();
	
	forms_collection_traversal_helper (doc->children, coll);

	return coll;
}

static void
applets_collection_traversal_helper (xmlNode *node, GdomeHTMLCollection *coll)
{
	xmlNode *n;

	n = node->children;
	while (n) {
		if (n->type == XML_ELEMENT_NODE) {
			if (strcmp (n->name, "applet") == 0) {
				gdome_xml_HTMLCollection_append (coll, gdome_xml_n_mkref (n));
			}
			else if (strcmp (n->name, "object") == 0) {
				char *prop = xmlGetProp (node, "codetype");
				
				if (strcasecmp (prop, "application/java") == 0) {
					/* We have a match */
					gdome_xml_HTMLCollection_append (coll, gdome_xml_n_mkref (n));
				}
				xmlFree (prop);
			}
		}
		applets_collection_traversal_helper (n, coll);

		n = n->next;
	}
}

GdomeHTMLCollection *
gdome_xml_HTMLDocument_applets (GdomeHTMLDocument *self, GdomeException *exc)
{
	Gdome_xml_Node *priv = (Gdome_xml_Node *)self;
	GdomeHTMLCollection *coll;
	xmlDoc *doc = NULL;
	
	if (self == NULL)
		return NULL;
	
	doc = (xmlDoc *)priv->n;
	
	if (doc == NULL)
		return NULL;
	if (doc->children == NULL)
		return NULL;
	
	coll = gdome_xml_HTMLCollection_mkref ();
	
	applets_collection_traversal_helper (doc->children, coll);

	return coll;
}

static void
anchors_collection_traversal_helper (xmlNode *node, GdomeHTMLCollection *coll)
{
	xmlNode *n;
	
	n = node->children;
	while (n) {
		if (n->type == XML_ELEMENT_NODE) {
			if (strcmp ("a", n->name) == 0) {
				char *prop = xmlGetProp (node, "name");
				
				if (strcmp (prop, "name") == 0) {
					/* We have a match */
					gdome_xml_HTMLCollection_append (coll, gdome_xml_n_mkref (n));
				}
				xmlFree (prop);
			}
		}
		anchors_collection_traversal_helper (n, coll);
		n = n->next;
	}
}

GdomeHTMLCollection *
gdome_xml_HTMLDocument_anchors (GdomeHTMLDocument *self, GdomeException *exc)
{
	Gdome_xml_Node *priv = (Gdome_xml_Node *)self;
	GdomeHTMLCollection *coll;
	xmlDoc *doc = NULL;
	
	if (self == NULL)
		return NULL;
	
	doc = (xmlDoc *)priv->n;
	
	if (doc == NULL)
		return NULL;
	if (doc->children == NULL)
		return NULL;
	
	coll = gdome_xml_HTMLCollection_mkref ();
	
	anchors_collection_traversal_helper (doc->children, coll);

	return coll;
}

static void
images_collection_traversal_helper (xmlNode *node, GdomeHTMLCollection *coll)
{
	xmlNode *n;
	
	n = node->children;
	
	while (n) {
		if (n->type == XML_ELEMENT_NODE) {
			if (strcmp (n->name, "img") == 0) {
				/* We have a match */
				gdome_xml_HTMLCollection_append (coll, gdome_xml_n_mkref (n));
			}
		}
		
		images_collection_traversal_helper (n, coll);
		
	}
	
	n = n->next;
}

GdomeHTMLCollection *
gdome_xml_HTMLDocument_images (GdomeHTMLDocument *self, GdomeException *exc)
{
	Gdome_xml_Node *priv = (Gdome_xml_Node *)self;
	GdomeHTMLCollection *coll;
	xmlDoc *doc = NULL;

	if (self == NULL)
		return NULL;
	
	doc = (xmlDoc *)priv->n;
	
	if (doc == NULL)
		return NULL;
	if (doc->children == NULL)
		return NULL;
	
	coll = gdome_xml_HTMLCollection_mkref ();
	
	images_collection_traversal_helper (doc->children, coll);

	return coll;
}

GdomeNodeList *
gdome_xml_HTMLDocument_getElementsByName (GdomeHTMLDocument *self, GdomeDOMString *elementName, GdomeException *exc)
{
	Gdome_xml_Node *priv = (Gdome_xml_Node *)self;
	GdomeNodeList *list;
	xmlDoc *doc = NULL;

	if (self == NULL)
		return NULL;
	
	doc = (xmlDoc *)priv->n;

	if (doc == NULL)
		return NULL;
	if (doc->children == NULL)
		return NULL;
	
	list = gdome_xml_nl_mkref ();
	gdome_xml_HTMLDocument_traversal_helper (doc->children, list, elementName->str);

	return list;
}
