/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*- */
/* gdome-xml-htmlcollection.c
 *
 * Copyright (C) 1999 Raph Levien <raph@acm.org>
 * Copyright (C) 2000 Mathieu Lacage <mathieu@gnu.org>
 * Copyright (C) 2000 Anders Carlsson <andersca@gnu.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <glib.h>
#include <libxml/tree.h>
#include <libxml/parser.h>
#include <libxml/xmlmemory.h>
#include "gdome-xml-htmlcollection.h"
#include "gdome-xml-node.h"
#include "gdome-xml-str.h"
#include "gdome-xml-element.h"

const GdomeHTMLCollectionVtab gdome_xml_HTMLCollection_vtab = {
	gdome_xml_HTMLCollection_ref,
	gdome_xml_HTMLCollection_unref,
	gdome_xml_HTMLCollection_query_interface,
	gdome_xml_HTMLCollection_length,
	gdome_xml_HTMLCollection_item,
	gdome_xml_HTMLCollection_namedItem
};

typedef struct _Gdome_xml_HTMLCollectionItem Gdome_xml_HTMLCollectionItem;

struct _Gdome_xml_HTMLCollectionItem {
	GdomeNode *node;
	Gdome_xml_HTMLCollectionItem *next;
};

struct _Gdome_xml_HTMLCollection {
	GdomeHTMLCollection super;
	int refcnt;
	Gdome_xml_HTMLCollectionItem *items;
};


GdomeHTMLCollection *
gdome_xml_HTMLCollection_mkref ()
{
	Gdome_xml_HTMLCollection *result = NULL;

	result = g_new (Gdome_xml_HTMLCollection, 1);
	result->refcnt = 1;
	result->items = NULL;
	result->super.vtab = &gdome_xml_HTMLCollection_vtab;

	return (GdomeHTMLCollection *)result;
	
}

void
gdome_xml_HTMLCollection_append (GdomeHTMLCollection *self, GdomeNode *node)
{
	Gdome_xml_HTMLCollection *priv = (Gdome_xml_HTMLCollection *)self;
	Gdome_xml_HTMLCollectionItem *item, *itemlist;

	if (self == NULL)
		return;

	if (priv->items == NULL) {
		/* Create a new item */
		item = g_new (Gdome_xml_HTMLCollectionItem, 1);
		item->next = NULL;
		item->node = node;
	}
	else {
		/* Move to the last item in the list */
		itemlist = priv->items;
		while (itemlist->next != NULL)
			itemlist = itemlist->next;

		item = g_new (Gdome_xml_HTMLCollectionItem, 1);
		item->next = NULL;
		item->node = node;
		itemlist->next = item;
	}
}

void
gdome_xml_HTMLCollection_ref (GdomeHTMLCollection *self, GdomeException *exc)
{
	Gdome_xml_HTMLCollection *priv;
	
	if (self == NULL)
		return;

	priv = (Gdome_xml_HTMLCollection *)self;

	priv->refcnt ++;
	
}

void
gdome_xml_HTMLCollection_unref (GdomeHTMLCollection *self, GdomeException *exc)
{
	Gdome_xml_HTMLCollection *priv;
	
	if (self == NULL)
		return;

	priv = (Gdome_xml_HTMLCollection *)self;

	priv->refcnt--;

	if (priv->refcnt == 0) {
		Gdome_xml_HTMLCollectionItem *item = priv->items;
		Gdome_xml_HTMLCollectionItem *bk_item;
		
		while (item != NULL) {
			gdome_xml_n_unref (item->node, exc);
			bk_item = item;
			item = item->next;
			g_free (bk_item);
		}
		g_free (self);
	}
}

void *
gdome_xml_HTMLCollection_query_interface (GdomeHTMLCollection *self, const char *interface, GdomeException *exc)
{
	return NULL;
}

unsigned long
gdome_xml_HTMLCollection_length (GdomeHTMLCollection *self, GdomeException *exc)
{
	unsigned long length = 0;
	Gdome_xml_HTMLCollection *priv = (Gdome_xml_HTMLCollection *)self;
	Gdome_xml_HTMLCollectionItem *item;
	
	if (self == NULL)
		return 0;
	if (exc == NULL)
		return 0;
	
	item = priv->items;
	
	for (length = 0; item != NULL; length++)
		item = item->next;

	return length;
}

GdomeNode *
gdome_xml_HTMLCollection_item (GdomeHTMLCollection *self, unsigned long index, GdomeException *exc)
{
	Gdome_xml_HTMLCollection *priv = (Gdome_xml_HTMLCollection *)self;
	Gdome_xml_HTMLCollectionItem *item;
	unsigned long i = 0;

	if (self == NULL)
		return 0;
	if (index < 0)
		return NULL;
	if (exc == NULL)
		return NULL;

	item = priv->items;
	
	for (i = 0; item != NULL && i < index; i++)
		item = item->next;

	if (item == NULL)
		return NULL;
	
	gdome_xml_n_ref (item->node, exc);
	return item->node;
}

GdomeNode *
gdome_xml_HTMLCollection_namedItem (GdomeHTMLCollection *self, GdomeDOMString *name, GdomeException *exc)
{
	Gdome_xml_HTMLCollection *priv = (Gdome_xml_HTMLCollection *)self;
	Gdome_xml_HTMLCollectionItem *item;
	GdomeDOMString *id, *elemid;
	
	if (self == NULL)
		return NULL;
	if (exc == NULL)
		return NULL;
	if (name == NULL)
		return NULL;

	/* First, look for an id */
	item = priv->items;

	id = gdome_xml_str_mkref_own ("id");
	while (item != NULL) {
		elemid = gdome_xml_el_getAttribute ((GdomeElement *)self, id, exc);

		if (elemid) {
			if (strcmp (elemid->str, name->str) == 0) {
				gdome_xml_str_unref1 (elemid);
				break;
			}
			gdome_xml_str_unref1 (elemid);
		}
		
		item = item->next;
	}
	
	if (item) {
		gdome_xml_n_ref (item->node, exc);
		return item->node;
	}

	/* We haven't found anything yet, so
		 look for elements with a name attribute */
	item = priv->items;

	id = gdome_xml_str_mkref_own ("name");

	while (item != NULL) {
		elemid = gdome_xml_el_getAttribute ((GdomeElement *)self, id, exc);
		
		if (elemid) {
			if (strcmp (elemid->str, name->str) == 0) {
				gdome_xml_str_unref1 (elemid);

				/* We've found an element where the NAME attribute matches.
				 * However, not all elements have this attribute, so check if
				 * the current element has it.
				 */
				elemid = gdome_xml_el_tagName ((GdomeElement *)self, exc);
				if (strcmp (elemid->str, "meta") == 0 ||
						strcmp (elemid->str, "form") == 0 ||
						strcmp (elemid->str, "select") == 0 ||
						strcmp (elemid->str, "input") == 0 ||
						strcmp (elemid->str, "textarea") == 0 ||
						strcmp (elemid->str, "button") == 0 ||
						strcmp (elemid->str, "a") == 0 ||
						strcmp (elemid->str, "img") == 0 ||
						strcmp (elemid->str, "object") == 0 ||
						strcmp (elemid->str, "applet") == 0 ||
						strcmp (elemid->str, "map") == 0 ||
						strcmp (elemid->str, "frame") == 0 ||
						strcmp (elemid->str, "iframe") == 0) {
					gdome_xml_str_unref1 (elemid);
					break;
				}
			}
			gdome_xml_str_unref1 (elemid);
		}
		item = item->next;
	}

	if (item) {
		gdome_xml_n_ref (item->node, exc);
		return item->node;
	}

	return NULL;
}

