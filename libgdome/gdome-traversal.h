/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* gdome-traversal.h
 *
 * Copyright (C) 2002 Paolo Casarini <paolo@casarini.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef GDOME_TRAVERSAL_H
#define GDOME_TRAVERSAL_H

#include <gdome.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef struct _GdomeNodeFilterVtab GdomeNodeFilterVtab;
typedef struct _GdomeNodeIteratorVtab GdomeNodeIteratorVtab;
typedef struct _GdomeTreeWalkerVtab GdomeTreeWalkerVtab;

typedef enum {
  GDOME_FILTER_ERROR = 0,
	GDOME_FILTER_ACCEPT = 1,
  GDOME_FILTER_REJECT = 2,
  GDOME_FILTER_SKIP =3
} GdomeFilterResultType;

typedef enum {
	GDOME_SHOW_ALL =                    0xFFFFFFFF,
	GDOME_SHOW_ELEMENT =                0x00000001,
	GDOME_SHOW_ATTRIBUTE =              0x00000002,
	GDOME_SHOW_TEXT =                   0x00000004,
	GDOME_SHOW_CDATA_SECTION =          0x00000008,
	GDOME_SHOW_ENTITY_REFERENCE =       0x00000010,
	GDOME_SHOW_ENTITY =                 0x00000020,
	GDOME_SHOW_PROCESSING_INSTRUCTION = 0x00000040,
	GDOME_SHOW_COMMENT =                0x00000080,
	GDOME_SHOW_DOCUMENT =               0x00000100,
	GDOME_SHOW_DOCUMENT_TYPE =          0x00000200,
	GDOME_SHOW_DOCUMENT_FRAGMENT =      0x00000400,
	GDOME_SHOW_NOTATION =               0x00000800
} GdomeWhatToShowType;

struct _GdomeNodeFilter {
	gpointer user_data;
};

struct _GdomeNodeIterator {
	gpointer user_data;
};

struct _GdomeTreeWalker {
	gpointer user_data;
};

/* ------------------------  NodeFilter  ------------------------ */
GdomeNodeFilter *gdome_nf_mkref (short (*callback) (GdomeNodeFilter *self, GdomeNode *n, GdomeException *exc));
void gdome_nf_ref (GdomeNodeFilter *self, GdomeException *exc);
void gdome_nf_unref (GdomeNodeFilter *self, GdomeException *exc);
gpointer gdome_nf_query_interface (GdomeNodeFilter *self, const char *interface, GdomeException *exc);
short gdome_nf_acceptNode (GdomeNodeFilter *self, GdomeNode *n, GdomeException *exc);

/* ----------------------  NodeIterator  ------------------------ */
GdomeNode *gdome_ni_root (GdomeNodeIterator *self, GdomeException *exc);
guint32 gdome_ni_whatToShow (GdomeNodeIterator *self, GdomeException *exc);
GdomeNodeFilter *gdome_ni_filter (GdomeNodeIterator *self, GdomeException *exc);
GdomeBoolean gdome_ni_expandEntityReference (GdomeNodeIterator *self, GdomeException *exc);
void gdome_ni_ref (GdomeNodeIterator *self, GdomeException *exc);
void gdome_ni_unref (GdomeNodeIterator *self, GdomeException *exc);
gpointer gdome_ni_query_interface (GdomeNodeIterator *self, const char *interface, GdomeException *exc);
GdomeNode *gdome_ni_nextNode (GdomeNodeIterator *self, GdomeException *exc);
GdomeNode *gdome_ni_previousNode (GdomeNodeIterator *self, GdomeException *exc);
void gdome_ni_detach (GdomeNodeIterator *self, GdomeException *exc);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* GDOME_TRAVERSAL_H */
