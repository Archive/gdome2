/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/* gdome-html.c
 *
 * Copyright (C) 1999 Raph Levien <raph@acm.org>
 * Copyright (C) 2000 Mathieu Lacage <mathieu@gnu.org>
 * Copyright (C) 2000 Anders Carlsson <andersca@gnu.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* -- gdome-html.c --- */
#include "gdome-html.h"

void *
gdome_HTMLCollection_query_interface (GdomeHTMLCollection *self, const char *interface, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->query_interface (self, interface, exc);
}

unsigned long
gdome_HTMLCollection_length (GdomeHTMLCollection *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->length (self, exc);
}

GdomeNode *
gdome_HTMLCollection_item (GdomeHTMLCollection *self, unsigned long index, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->item (self, index, exc);
}

GdomeNode *
gdome_HTMLCollection_namedItem (GdomeHTMLCollection *self, GdomeDOMString *name, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->namedItem (self, name, exc);
}

GdomeHTMLDocument *
gdome_HTMLDOMImplementation_createHTMLDocument (GdomeHTMLDOMImplementation *self, GdomeDOMString *title, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->createHTMLDocument (self, title, exc);
}

/* gen_invoker_super GdomeHTMLDOMImplementation HTMLDOMImplementation HTMLDOMImplementation: DOMImplementation ()*/
void
gdome_HTMLDOMImplementation_ref (GdomeHTMLDOMImplementation *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.ref ((GdomeDOMImplementation *)self, exc);
}

void
gdome_HTMLDOMImplementation_unref (GdomeHTMLDOMImplementation *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.unref ((GdomeDOMImplementation *)self, exc);
}

void *
gdome_HTMLDOMImplementation_query_interface (GdomeHTMLDOMImplementation *self, const char *interface, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.query_interface ((GdomeDOMImplementation *)self, interface, exc);
}

GdomeBoolean
gdome_HTMLDOMImplementation_hasFeature (GdomeHTMLDOMImplementation *self, GdomeDOMString *feature, GdomeDOMString *version, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.hasFeature ((GdomeDOMImplementation *)self, feature, version, exc);
}

GdomeDocumentType *
gdome_HTMLDOMImplementation_createDocumentType (GdomeHTMLDOMImplementation *self, GdomeDOMString *qualifiedName, GdomeDOMString *publicId, GdomeDOMString *systemId, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.createDocumentType ((GdomeDOMImplementation *)self, qualifiedName, publicId, systemId, exc);
}

GdomeDocument *
gdome_HTMLDOMImplementation_createDocument (GdomeHTMLDOMImplementation *self, GdomeDOMString *namespaceURI, GdomeDOMString *qualifiedName, GdomeDocumentType *doctype, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.createDocument ((GdomeDOMImplementation *)self, namespaceURI, qualifiedName, doctype, exc);
}

GdomeDOMString *
gdome_HTMLDocument_title (GdomeHTMLDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->title (self, exc);
}

void
gdome_HTMLDocument_set_title (GdomeHTMLDocument *self, GdomeDOMString *title, GdomeException *exc)
{
  *exc = 0;
  self->vtab->set_title (self, title, exc);
}

GdomeDOMString *
gdome_HTMLDocument_referrer (GdomeHTMLDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->referrer (self, exc);
}

GdomeDOMString *
gdome_HTMLDocument_domain (GdomeHTMLDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->domain (self, exc);
}

GdomeDOMString *
gdome_HTMLDocument_URL (GdomeHTMLDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->URL (self, exc);
}

GdomeHTMLElement *
gdome_HTMLDocument_body (GdomeHTMLDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->body (self, exc);
}

void
gdome_HTMLDocument_set_body (GdomeHTMLDocument *self, GdomeHTMLElement *body, GdomeException *exc)
{
  *exc = 0;
  self->vtab->set_body (self, body, exc);
}

GdomeHTMLCollection *
gdome_HTMLDocument_images (GdomeHTMLDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->images (self, exc);
}

GdomeHTMLCollection *
gdome_HTMLDocument_applets (GdomeHTMLDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->images (self, exc);
}

GdomeHTMLCollection *
gdome_HTMLDocument_links (GdomeHTMLDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->images (self, exc);
}

GdomeHTMLCollection *
gdome_HTMLDocument_forms (GdomeHTMLDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->images (self, exc);
}

GdomeHTMLCollection *
gdome_HTMLDocument_anchors (GdomeHTMLDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->images (self, exc);
}

GdomeDOMString *
gdome_HTMLDocument_cookie (GdomeHTMLDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->cookie (self, exc);
}

void
gdome_HTMLDocument_set_cookie (GdomeHTMLDocument *self, GdomeDOMString *cookie, GdomeException *exc)
{
  *exc = 0;
  self->vtab->set_cookie (self, cookie, exc);
}

void
gdome_HTMLDocument_open (GdomeHTMLDocument *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->open (self, exc);
}

void
gdome_HTMLDocument_close (GdomeHTMLDocument *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->close (self, exc);
}

void
gdome_HTMLDocument_write (GdomeHTMLDocument *self, GdomeDOMString *text, GdomeException *exc)
{
  *exc = 0;
  self->vtab->write (self, text, exc);
}

void
gdome_HTMLDocument_writeln (GdomeHTMLDocument *self, GdomeDOMString *text, GdomeException *exc)
{
  *exc = 0;
  self->vtab->writeln (self, text, exc);
}

GdomeNodeList *
gdome_HTMLDocument_getElementsByName (GdomeHTMLDocument *self, GdomeDOMString *elementName, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->getElementsByName (self, elementName, exc);
}

/* gen_invoker_super GdomeHTMLDocument HTMLDocument HTMLDocument: Document ()*/
GdomeDocumentType *
gdome_HTMLDocument_doctype (GdomeHTMLDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.doctype ((GdomeDocument *)self, exc);
}

GdomeDOMImplementation *
gdome_HTMLDocument_implementation (GdomeHTMLDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.implementation ((GdomeDocument *)self, exc);
}

GdomeElement *
gdome_HTMLDocument_documentElement (GdomeHTMLDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.documentElement ((GdomeDocument *)self, exc);
}

GdomeElement *
gdome_HTMLDocument_createElement (GdomeHTMLDocument *self, GdomeDOMString *tagName, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.createElement ((GdomeDocument *)self, tagName, exc);
}

GdomeDocumentFragment *
gdome_HTMLDocument_createDocumentFragment (GdomeHTMLDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.createDocumentFragment ((GdomeDocument *)self, exc);
}

GdomeText *
gdome_HTMLDocument_createTextNode (GdomeHTMLDocument *self, GdomeDOMString *data, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.createTextNode ((GdomeDocument *)self, data, exc);
}

GdomeComment *
gdome_HTMLDocument_createComment (GdomeHTMLDocument *self, GdomeDOMString *data, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.createComment ((GdomeDocument *)self, data, exc);
}

GdomeCDATASection *
gdome_HTMLDocument_createCDATASection (GdomeHTMLDocument *self, GdomeDOMString *data, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.createCDATASection ((GdomeDocument *)self, data, exc);
}

GdomeProcessingInstruction *
gdome_HTMLDocument_createProcessingInstruction (GdomeHTMLDocument *self, GdomeDOMString *target, GdomeDOMString *data, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.createProcessingInstruction ((GdomeDocument *)self, target, data, exc);
}

GdomeAttr *
gdome_HTMLDocument_createAttribute (GdomeHTMLDocument *self, GdomeDOMString *name, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.createAttribute ((GdomeDocument *)self, name, exc);
}

GdomeEntityReference *
gdome_HTMLDocument_createEntityReference (GdomeHTMLDocument *self, GdomeDOMString *name, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.createEntityReference ((GdomeDocument *)self, name, exc);
}

GdomeNodeList *
gdome_HTMLDocument_getElementsByTagName (GdomeHTMLDocument *self, GdomeDOMString *tagname, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.getElementsByTagName ((GdomeDocument *)self, tagname, exc);
}

GdomeNode *
gdome_HTMLDocument_importNode (GdomeHTMLDocument *self, GdomeNode *importedNode, GdomeBoolean deep, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.importNode ((GdomeDocument *)self, importedNode, deep, exc);
}

GdomeElement *
gdome_HTMLDocument_createElementNS (GdomeHTMLDocument *self, GdomeDOMString *namespaceURI, GdomeDOMString *qualifiedName, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.createElementNS ((GdomeDocument *)self, namespaceURI, qualifiedName, exc);
}

GdomeAttr *
gdome_HTMLDocument_createAttributeNS (GdomeHTMLDocument *self, GdomeDOMString *namespaceURI, GdomeDOMString *qualifiedName, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.createAttributeNS ((GdomeDocument *)self, namespaceURI, qualifiedName, exc);
}

GdomeNodeList *
gdome_HTMLDocument_getElementsByTagNameNS (GdomeHTMLDocument *self, GdomeDOMString *namespaceURI, GdomeDOMString *qualifiedName, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.getElementsByTagNameNS ((GdomeDocument *)self, namespaceURI, qualifiedName, exc);
}

GdomeElement *
gdome_HTMLDocument_getElementById (GdomeHTMLDocument *self, GdomeDOMString *elementId, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.getElementById ((GdomeDocument *)self, elementId, exc);
}

/* gen_invoker_super GdomeHTMLDocument HTMLDocument Document: Node (super.)*/
void
gdome_HTMLDocument_ref (GdomeHTMLDocument *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.super.ref ((GdomeNode *)self, exc);
}

void
gdome_HTMLDocument_unref (GdomeHTMLDocument *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.super.unref ((GdomeNode *)self, exc);
}

void *
gdome_HTMLDocument_query_interface (GdomeHTMLDocument *self, const char *interface, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.query_interface ((GdomeNode *)self, interface, exc);
}

GdomeDOMString *
gdome_HTMLDocument_nodeName (GdomeHTMLDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.nodeName ((GdomeNode *)self, exc);
}

GdomeDOMString *
gdome_HTMLDocument_nodeValue (GdomeHTMLDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.nodeValue ((GdomeNode *)self, exc);
}

void
gdome_HTMLDocument_set_nodeValue (GdomeHTMLDocument *self, GdomeDOMString *nodeValue, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.super.set_nodeValue ((GdomeNode *)self, nodeValue, exc);
}

unsigned short
gdome_HTMLDocument_nodeType (GdomeHTMLDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.nodeType ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_HTMLDocument_parentNode (GdomeHTMLDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.parentNode ((GdomeNode *)self, exc);
}

GdomeNodeList *
gdome_HTMLDocument_childNodes (GdomeHTMLDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.childNodes ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_HTMLDocument_firstChild (GdomeHTMLDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.firstChild ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_HTMLDocument_lastChild (GdomeHTMLDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.lastChild ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_HTMLDocument_previousSibling (GdomeHTMLDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.previousSibling ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_HTMLDocument_nextSibling (GdomeHTMLDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.nextSibling ((GdomeNode *)self, exc);
}

GdomeNamedNodeMap *
gdome_HTMLDocument_attributes (GdomeHTMLDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.attributes ((GdomeNode *)self, exc);
}

GdomeDocument *
gdome_HTMLDocument_ownerDocument (GdomeHTMLDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.ownerDocument ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_HTMLDocument_insertBefore (GdomeHTMLDocument *self, GdomeNode *newChild, GdomeNode *refChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.insertBefore ((GdomeNode *)self, newChild, refChild, exc);
}

GdomeNode *
gdome_HTMLDocument_replaceChild (GdomeHTMLDocument *self, GdomeNode *newChild, GdomeNode *oldChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.replaceChild ((GdomeNode *)self, newChild, oldChild, exc);
}

GdomeNode *
gdome_HTMLDocument_removeChild (GdomeHTMLDocument *self, GdomeNode *oldChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.removeChild ((GdomeNode *)self, oldChild, exc);
}

GdomeNode *
gdome_HTMLDocument_appendChild (GdomeHTMLDocument *self, GdomeNode *newChild, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.appendChild ((GdomeNode *)self, newChild, exc);
}

GdomeBoolean
gdome_HTMLDocument_hasChildNodes (GdomeHTMLDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.hasChildNodes ((GdomeNode *)self, exc);
}

GdomeNode *
gdome_HTMLDocument_cloneNode (GdomeHTMLDocument *self, GdomeBoolean deep, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.cloneNode ((GdomeNode *)self, deep, exc);
}

void
gdome_HTMLDocument_normalize (GdomeHTMLDocument *self, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.super.normalize ((GdomeNode *)self, exc);
}

GdomeBoolean
gdome_HTMLDocument_isSupported (GdomeHTMLDocument *self, GdomeDOMString *feature, GdomeDOMString *version, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.isSupported ((GdomeNode *)self, feature, version, exc);
}

GdomeDOMString *
gdome_HTMLDocument_namespaceURI (GdomeHTMLDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.namespaceURI ((GdomeNode *)self, exc);
}

GdomeDOMString *
gdome_HTMLDocument_prefix (GdomeHTMLDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.prefix ((GdomeNode *)self, exc);
}

void
gdome_HTMLDocument_set_prefix (GdomeHTMLDocument *self, GdomeDOMString *prefix, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.super.set_prefix ((GdomeNode *)self, prefix, exc);
}

GdomeDOMString *
gdome_HTMLDocument_localName (GdomeHTMLDocument *self, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.localName ((GdomeNode *)self, exc);
}

void
gdome_HTMLDocument_addEventListener (GdomeHTMLDocument *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.super.addEventListener ((GdomeNode *)self, type, listener, useCapture, exc);
}

void
gdome_HTMLDocument_removeEventListener (GdomeHTMLDocument *self, GdomeDOMString *type, GdomeEventListener *listener, GdomeBoolean useCapture, GdomeException *exc)
{
  *exc = 0;
  self->vtab->super.super.removeEventListener ((GdomeNode *)self, type, listener, useCapture, exc);
}

GdomeBoolean
gdome_HTMLDocument_dispatchEvent (GdomeHTMLDocument *self, GdomeEvent *evt, GdomeException *exc)
{
  *exc = 0;
  return self->vtab->super.super.dispatchEvent ((GdomeNode *)self, evt, exc);
}

