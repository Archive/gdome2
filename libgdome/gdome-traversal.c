/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 2; tab-width: 2 -*- */
/* gdome-traversal.c
 *
 * CopyRight (C) 2002 Paolo Casarini <paolo@casarini.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "gdome-traversal.h"
#include "gdome-trv-nodeiterator.h"
#include "gdome-trv-nodefilter.h"
#include "gdome-trv-treewalker.h"


/******************************************************************************
          GdomeNodeFilter interface API
 ******************************************************************************/
/**
 * gdome_nf_mkref:
 * @callback:  the callback function that is to be called when
 *             gdome_nf_acceptNode() is called.
 *
 * Returns: a new allocated #GdomeNodeFilter.
 */
GdomeNodeFilter *
gdome_nf_mkref (short (*callback) (GdomeNodeFilter *self, GdomeNode *n, GdomeException *exc))
{
	return gdome_trv_nf_mkref (callback);
}
/**
 * gdome_nf_ref:
 * @self:  NodeFilter Object ref
 * @exc:  Exception Object ref
 *
 * Increase the reference count of the specified NodeFilter.
 */
void
gdome_nf_ref (GdomeNodeFilter *self, GdomeException *exc)
{
	if (self == NULL) {
		*exc = GDOME_NULL_POINTER_ERR;
		return;
	}
	*exc = 0;
	((Gdome_trv_NodeFilter *)self)->vtab->ref (self, exc);
}
/**
 * gdome_nf_unref:
 * @self:  NodeFilter Object ref
 * @exc:  Exception Object ref
 *
 * Decrease the reference count of the specified NodeFilter. Free the
 * NodeFilter structure if the NodeFilter will have zero reference.
 */
void
gdome_nf_unref (GdomeNodeFilter *self, GdomeException *exc)
{
	if (self == NULL) {
		*exc = GDOME_NULL_POINTER_ERR;
		return;
	}
	*exc = 0;
	((Gdome_trv_NodeFilter *)self)->vtab->unref (self, exc);
}
/**
 * gdome_nf_query_interface:
 * @self:  NodeFilter Object ref
 * @interface:  name of the Interface needed.
 * @exc:  Exception Object ref
 *
 * Returns: a reference to the object that implements the interface needed or
 * %NULL if this object doesn't implement the interface specified.
 */
gpointer
gdome_nf_query_interface (GdomeNodeFilter *self, const char *interface, GdomeException *exc)
{
	if (self == NULL) {
		*exc = GDOME_NULL_POINTER_ERR;
		return NULL;
	}
	*exc = 0;
	return ((Gdome_trv_NodeFilter *)self)->vtab->query_interface (self, interface, exc);
}
/**
 * gdome_nf_acceptNode:
 * @self:  NodeFilter Object ref
 * @n:  The node to check to see if it passes the filter or not..
 * @exc:  Exception Object ref
 *
 * Test whether a specified node is visible in the logical view of a
 * #GdomeTreeWalker or #GdomeNodeIterator. This function will be called by the
 * implementation of #GdomeTreeWalker and #GdomeNodeIterator; it is not
 * normally called directly from the user code. (Though you could do so if you
 * wanted to use the same filter to guide your own application logic.)
 *
 * Returns: a costant to determine wheter the node is accepted, rejected or
 * skipped. In particular it returns %GDOME_FILTER_ACCEPT or
 * %GDOME_FILTER_REJECT or %GDOME_FILTER_SKIP.
 */
short
gdome_nf_acceptNode (GdomeNodeFilter *self, GdomeNode *n, GdomeException *exc)
{
	if (self == NULL) {
		*exc = GDOME_NULL_POINTER_ERR;
		return 0;
	}
	*exc = 0;
	return ((Gdome_trv_NodeFilter *)self)->vtab->acceptNode (self, n, exc);
}

/******************************************************************************
          GdomeNodeIterator interface API
 ******************************************************************************/
/**
 * gdome_ni_root:
 * @self: NodeIterator Object ref
 * @exc:  Exception Object ref
 *
 * Returns: The root node of the NodeIterator, as specified when it was created
 */
GdomeNode *
gdome_ni_root (GdomeNodeIterator *self, GdomeException *exc)
{
	if (self == NULL) {
		*exc = GDOME_NULL_POINTER_ERR;
		return NULL;
	}
	*exc = 0;
	return ((Gdome_trv_NodeIterator *)self)->vtab->root (self, exc);
}
/**
 * gdome_ni_whatToShow:
 * @self: NodeIterator Object ref
 * @exc:  Exception Object ref
 *
 * Returns: This attribute determines which node types are presented via the
 * iterator. The available set of costants is defined in #GdomeWhatToShowType.
 * Nodes not accepted by @whatToShow will be skipped, baut their children may
 * still be considered. Note that this skip takes precedence over the filter,
 * if any.
 */
guint32
gdome_ni_whatToShow (GdomeNodeIterator *self, GdomeException *exc)
{
	if (self == NULL) {
		*exc = GDOME_NULL_POINTER_ERR;
		return 0;
	}
	*exc = 0;
	return ((Gdome_trv_NodeIterator *)self)->vtab->whatToShow (self, exc);
}
/**
 * gdome_ni_filter:
 * @self: NodeIterator Object ref
 * @exc:  Exception Object ref
 *
 * Returns: The #GdomeNodeFilter used to screen nodes.
 */
GdomeNodeFilter *
gdome_ni_filter (GdomeNodeIterator *self, GdomeException *exc)
{
	if (self == NULL) {
		*exc = GDOME_NULL_POINTER_ERR;
		return NULL;
	}
	*exc = 0;
	return ((Gdome_trv_NodeIterator *)self)->vtab->filter (self, exc);
}
/**
 * gdome_ni_expandEntityReference:
 * @self: NodeIterator Object ref
 * @exc:  Exception Object ref
 *
 * Returns: %TRUE if the entityReferenceExpansion flag is set, %FALSE
 * otherwise.
 */
GdomeBoolean
gdome_ni_expandEntityReference (GdomeNodeIterator *self, GdomeException *exc)
{
	if (self == NULL) {
		*exc = GDOME_NULL_POINTER_ERR;
		return FALSE;
	}
	*exc = 0;
	return ((Gdome_trv_NodeIterator *)self)->vtab->expandEntityReference (self, exc);
}
/**
 * gdome_ni_ref:
 * @self:  NodeIterator Object ref
 * @exc:  Exception Object ref
 *
 * Increase the reference count of the specified NodeIterator.
 */
void
gdome_ni_ref (GdomeNodeIterator *self, GdomeException *exc)
{
	if (self == NULL) {
		*exc = GDOME_NULL_POINTER_ERR;
		return;
	}
	*exc = 0;
	((Gdome_trv_NodeIterator *)self)->vtab->ref (self, exc);
}
/**
 * gdome_ni_unref:
 * @self:  NodeIterator Object ref
 * @exc:  Exception Object ref
 *
 * Decrease the reference count of the specified NodeIterator. Free the
 * NodeIterator structure if the NodeIterator will have zero reference.
 */
void
gdome_ni_unref (GdomeNodeIterator *self, GdomeException *exc)
{
	if (self == NULL) {
		*exc = GDOME_NULL_POINTER_ERR;
		return;
	}
	*exc = 0;
	((Gdome_trv_NodeIterator *)self)->vtab->unref (self, exc);
}
/**
 * gdome_ni_query_interface:
 * @self:  NodeIterator Object ref
 * @interface:  name of the Interface needed.
 * @exc:  Exception Object ref
 *
 * Returns: a reference to the object that implements the interface needed or
 * %NULL if this object doesn't implement the interface specified.
 */
gpointer
gdome_ni_query_interface (GdomeNodeIterator *self, const char *interface, GdomeException *exc)
{
	if (self == NULL) {
		*exc = GDOME_NULL_POINTER_ERR;
		return NULL;
	}
	*exc = 0;
	return ((Gdome_trv_NodeIterator *)self)->vtab->query_interface (self, interface, exc);
}
/**
 * gdome_ni_nextNode:
 * @self: NodeIterator Object ref
 * @exc:  Exception Object ref
 *
 * Returns the next node in the set and advances the position of the
 * iterator in the set. After a #GdomeNodeIterator is created, the first
 * call to gdome_ni_nextNode() returns the first node in the set.
 *
 * %GDOME_INVALID_STATE_ERR: Raised if this methid is called after the
 * gdome_ni_detach() method was invoked.
 * Returns: the next #GdomeNode in the set being iterated over, or %NULL if
 * there are no more members in that set.
 */
GdomeNode *
gdome_ni_nextNode (GdomeNodeIterator *self, GdomeException *exc)
{
	if (self == NULL) {
		*exc = GDOME_NULL_POINTER_ERR;
		return NULL;
	}
	*exc = 0;
	return ((Gdome_trv_NodeIterator *)self)->vtab->nextNode (self, exc);
}
/**
 * gdome_ni_previousNode:
 * @self: NodeIterator Object ref
 * @exc:  Exception Object ref
 *
 * Returns the previous node in the set and moves the position of the
 * #GdomeNodeIterator backwards in the set.
 *
 * %GDOME_INVALID_STATE_ERR: Raised if this methid is called after the
 * gdome_ni_detach() method was invoked.
 * Returns: the previous #GdomeNode in the set being iterated over, or %NULL
 * if there are no more members in that set.
 */
GdomeNode *
gdome_ni_previousNode (GdomeNodeIterator *self, GdomeException *exc)
{
	if (self == NULL) {
		*exc = GDOME_NULL_POINTER_ERR;
		return NULL;
	}
	*exc = 0;
	return ((Gdome_trv_NodeIterator *)self)->vtab->previousNode (self, exc);
}
/**
 * gdome_ni_detach:
 * @self: NodeIterator Object ref
 * @exc:  Exception Object ref
 *
 * Detaches the #GdomeNodeIterator from the set which it iterated over,
 * releasing any computational resourses and placing the iterator in
 * INVALID state. After gdome_ni_detach() has been invoked, calls to
 * gdome_ni_nextNode() and gdome_ni_previousNode() will raise the exception
 * %GDOME_INVALID_STATE_ERR.
 */
void
gdome_ni_detach (GdomeNodeIterator *self, GdomeException *exc)
{
	if (self == NULL) {
		*exc = GDOME_NULL_POINTER_ERR;
		return;
	}
	*exc = 0;
	((Gdome_trv_NodeIterator *)self)->vtab->detach (self, exc);
}
